package bot;

import java.util.Arrays;

/**
 * ChatnetUtils contains a ChatnetProtocol tokenizer useful for parsing chatnet
 * 		messages into arrays of tokens
 * ChatnetUtils appropriately splits chatnet messages into their respective
 * 		number of tokens
 * 		- a simple split with delimiter ':' may not work well as an alternative
 * 		  since it is possible players may type ':' character in their messages
 * 		  which which over-split a message into more pieces than it needs to
 * 		- hence the need for such an ugly looking parsing function
 * @author Nax
 * @author Alex
 */

class ChatnetUtils {

	//chatNetTokenize: tokenizes current BufferedLine into correct amount of tokens
		//Receives: chatnet protocol string
		//Returns: string array with appropriate number of tokens
	String[] chatnetTokenize(String message)
		{
			String[] tokens = message.split(":",2);

			if(tokens[0] == null){
				System.err.println("chatNetTokenize cannot tokenize null references");
				return null;
			}

			switch( tokens[0] ) { 
			case "KILL":
				return message.split(":");
			case "MSG":
				tokens = message.split(":",3);
				switch (tokens[1]) {
					case "PUB":
					case "PRIV":
					case "CHAT":
					case "FREQ":
					case "MOD":
					case "PUBM":

						tokens = message.split(":", 4);
						if (tokens.length == 4) {
							return tokens;
						} else if (tokens.length == 3) {
							return new String[]{tokens[0], tokens[1], tokens[2], ""};
						}
						break;
					case "ARENA":
					case "CMD":
					case "SYSOP":
						tokens = message.split(":", 3);
						if (tokens.length == 3) {
							return tokens;
						} else if (tokens.length == 2) {
							return new String[]{tokens[0], tokens[1], ""};
						}
						break;
					case "SQUAD":
						tokens = message.split(":", 5);
						if (tokens.length == 5) {
							return tokens;
						} else if (tokens.length == 4) {
							return new String[]{tokens[0], tokens[1], tokens[2], tokens[3], ""};
						}
						break;
					default:
						System.err.println("\nError, MSG token unrecognized: " + message + "\n\ntokens:" + Arrays.toString(tokens) + "\n\n");
						return null;
				}
				tokensError(message, tokens);
				return null;

			case "SEND":
				tokens = message.split(":",3);
				switch (tokens[1]) {
					case "PRIV":
					case "FREQ":
					case "PRIVCMD":

						tokens = message.split(":", 4);
						if (tokens.length == 4) {
							return tokens;
						} else if (tokens.length == 3) {
							return new String[]{tokens[0], tokens[1], tokens[2], ""};
						}
						break;
					case "PUB":
					case "CMD":
					case "PUBM":
					case "MOD":
						tokens = message.split(":", 3);
						if (tokens.length == 3) {
							return tokens;
						} else if (tokens.length == 2) {
							return new String[]{tokens[0], tokens[1], ""};
						}
						break;
					case "CHAT":
						tokens = message.split(":", 3);
						if (tokens.length == 3) {
							int index = tokens[2].indexOf(';', 1);
							if (index == -1) {
								return new String[]{tokens[0], tokens[1], "1", tokens[2]};
							} else {
								return new String[]{tokens[0], tokens[1], tokens[2].split(";", 2)[0], tokens[2].split(";", 2)[1]};
							}
						} else if (tokens.length == 2) {
							return new String[]{tokens[0], tokens[1], "1", ""};
						}
						break;
					case "SQUAD":
						tokens = message.split(":", 5);
						if (tokens.length == 5) {
							return tokens;
						} else if (tokens.length == 4) {
							return new String[]{tokens[0], tokens[1], tokens[2], tokens[3], ""};
						}
						break;
					default:
						System.err.println("\nError, SEND token unrecognized: " + message + "\n\ntokens:" + Arrays.toString(tokens) + "\n\n");
						return null;
				}
				tokensError(message, tokens);
				return null;
			case "SHIPFREQCHANGE":
				return message.split(":");
			case "CHANGEFREQ" :
				return message.split(":");
			case "ENTERING" :
				return message.split(":");
			case "LEAVING" :
				return message.split(":");
			case "PLAYER" :
				return message.split(":");
			case "GO" :
				return message.split(":");
			case "INARENA" :
				return message.split(":");
			case "TERMINAL" :
				return message.split(":");
			case "NOOP" :
				return message.split(":");
			case "LOGIN" :
				return message.split(":");
			case "LOGINOK" :
				return message.split(":");
			case "LOGINBAD" :
				return message.split(":");
			}//end of switch

			System.err.println("\nError, message unrecognized: " + message + "\n\ntokens:" + Arrays.toString(tokens) + "\n\n");
			return null;
		}
		
	private void tokensError(String message, String[] tokens) {
		System.err.println("Error, tokens.length: " + tokens.length + "\nbuffer: " + message);
	}
}
