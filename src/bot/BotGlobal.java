package bot;

import java.util.HashSet;

import bot.arena.ArenaEntity;
import bot.arena.ArenaEntityManager;
import bot.duel.PlayerDuelStats;
import bot.duel.StatsSerialization;
import bot.ladder.TeamEntityManager;
import bot.statparser.Player;

public class BotGlobal {
	public static HashSet<PlayerDuelStats> playerPubDuelRatings = new HashSet<PlayerDuelStats>();
	private static boolean connected;
	static HashSet<Player> players = new HashSet<Player>();
	static PublicArenaController publicArenaController = new PublicArenaController(players);
	static ScrimArenaController scrimArenaController = new ScrimArenaController(players);

	public static TeamEntityManager ladderTeamManager = new TeamEntityManager();

	private static ArenaEntity scrimArena = new ArenaEntity();
	public static ArenaEntityManager scrimArenaManager = new ArenaEntityManager(scrimArena);
	
	private static BotGlobal instance = new BotGlobal();

	public static BotGlobal getInstance() {
		return instance;
	}
	
	static void setup() {
		BotGlobal.playerPubDuelRatings = StatsSerialization.loadStats();
		BotGlobal.ladderTeamManager.loadData();
	}

	static void setConnected(boolean connectionFlag) {
		BotGlobal.connected = true;
	}

	static boolean isConnected() {
		return connected;
	}

}
