package bot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

import bot.duel.StatsSerialization;


/**
 * ChatnetSocket takes care of TCP socket communication between Subspace zones 
 * 		running on ASSS software.
 * ChatnetSocket does not abstract the Chatnet Protocol itself.
 * 		- Messages received directly through the listening events this class 
 * 		  implements, will be in Chatnet Protocol format
 * 		- Messages sent directly through this socket should be in Chatnet 
 * 		  protocol format
 * @author Nax
 */

public class ChatnetSocket {
	private Socket rawSocket;
	
	private PrintWriter    socketOutput;
	private BufferedReader socketInput;
	
	private String userName;
	
	//Holds classes<ChatnetSocketListener> that enlist to listen to socket messages.
	private ArrayList<ChatnetSocketListener> listeners = new ArrayList<ChatnetSocketListener>();
	
	//Queues that hold output messages.
	private LinkedBlockingQueue<String> outputMessageQueue;
	
	//Holds last read line from queue stream
	private String BufferedLine;
	
	//Used to indicate connectionStatus information (such as reasons why connection failed)
	private String connectionStatus = "Not Connected";
	
	/* If this variable is true, input message events will not be fired, and will
			accumulate, until input listening is unpaused again
		Use pauseListening() and unPauseListening() methods to manipulate this variable.
			*/
	private boolean inputListeningIsPaused = false;
	
	ChatnetSocket() {
		
	}
	
	
	void connect(String IPAdress, int PortNumber, String username, String password, String description) throws
                                                                                                        IOException {
		try {
			   this.userName = username;
	           this.rawSocket = new Socket(IPAdress, PortNumber);						//create socket
	           this.socketOutput = new PrintWriter((this.rawSocket).getOutputStream(),true);	//create output stream
	           this.socketInput =  new BufferedReader( 
	        		   new InputStreamReader( (this.rawSocket).getInputStream()	));		//create input stream
	    } catch (UnknownHostException e) {
            connectionStatus = "unrecognized host";
            System.err.println("Do not recognize host: " + IPAdress + ":" + PortNumber);
            return;
        } catch (IOException e) {
            connectionStatus = "Server Problem. Cannot connect.";
            System.err.println("Couldn't get I/O for the connection to: " + IPAdress + ":" + PortNumber);
            return;
        }
		
		//now we create -input/output- message queues that will -store/feedFrom- the -input/output- stream
        this.outputMessageQueue = new LinkedBlockingQueue<String>();
        startInputListeningTimer();
		startOutputListeningTimer();
		timeoutTimer();
		
		chatnetLogin(username,password,description);

		return;
	}
	
	//Enlists newListener to receiving the socket's inputStream messages
	void addListener(ChatnetSocketListener newListener) {
        listeners.add(newListener);
    }
	
	//adds message to queue to be sent through socket
	void send(String message) throws IOException {
		outputMessageQueue.add(message+"\n");
		for(ChatnetSocketListener listener : listeners) {
            listener.processChatnetMessage(this, message, userName);
        }
	}
	
	//adds message to queue to be sent through socket without sending it to listeners
	void sendNoListen(String message)
	{
		outputMessageQueue.add(message+"\n");
	}
	
	//pauses the stream from sending messages to listeners
	public void pauseListening()
	{
		inputListeningIsPaused = true;
	}
	
	//unpauses the stream from sending messages to listeners
	public void unPauseListening()
	{
		inputListeningIsPaused = false;
	}
	
	//returns connection status, true if connected, false otherwise
	boolean isConnected() {
		if( connectionStatus.equals("connected"))
			return true;
		else
			return false;
	}
	
	//returns descriptive connection status in string form
	//useful when determining reason why connection failed (if connection failed)
	public String getConnectionStatus() {
        return connectionStatus;
	}
	
	
	//logs in to ss server using chatnet login protocol
	private void chatnetLogin(String username, String password, String description) throws IOException {
		send("LOGIN:1.6;" + description + ":" + username + ":" + password);
		if( getNextStreamMessage().startsWith("LOGINOK")) {
			connectionStatus = "connected";
			System.out.println("Login Successful");
		} else {
			if (BufferedLine.startsWith("LOGINBAD:the server is busy")) {
				connectionStatus = "invalid login. \n Biller has blocked you for too many invalid retries.\nTry again in a few hours.";
			} else {
				connectionStatus = "invalid login.";
			}
            close();
			System.out.println("LOGIN WAS BAD: " + BufferedLine);
		}
		return; 
	}
	
	/* closes both input/output streams
	 * closes both input/output listeners(queues)
	 * closes socket
	 */
	void close()
	{
		try {
			notifyListenersOfClose();
			
			connectionStatus = "disconnected";
			
			timeoutTimer.cancel();
			System.out.println("TIMER CLOSED: timeoutTimer");
			
			outputListeningTimer.cancel();
			System.out.println("TIMER CLOSED: outputTimer (socket outbound)");
			
			inputListeningTimer.cancel();
			System.out.println("TIMER CLOSED: inputTimer (socket inbound)");

			socketOutput.close();
			socketInput.close();
			
			rawSocket.close();
	    } catch (IOException e) {
	    	System.out.println("ChatnetSocket.close() IOException: " + e);
	    }
	}
	
	//Used for class socket messages, such as user authentication (before i/o queues have been initialized)
	private String getNextStreamMessage() 
	{
		try {
			BufferedLine = socketInput.readLine();
			return BufferedLine;
		} catch (IOException e) {
            System.err.println("getNextStreamMessage --> IOException:  " + e);
            return null;
        }
	}
	
	private boolean inputStreamIsReady() {
		try {
			return socketInput.ready();
		} catch (IOException e) {
			return false;
		}
	}
	
	//
	private void notifyListenersOfChatNetMessageReceived(String chatNetMessage) throws IOException {
        if(!this.isConnected() || listeners.size() == 0) {
        	return;
        }
		for(ChatnetSocketListener listener : listeners) {
            listener.processChatnetMessage(this, chatNetMessage, userName);
        }
    }
	
	private void notifyListenersOfClose() {
        if(listeners.size() == 0) {
        	return;
        }
		for(ChatnetSocketListener listener : listeners) {
            listener.chatnetSocketHasClosed(this);
        }
    }

	/*-------------------------------------------------------------------------*
	 * SECTION: Timers                                                         *
	 *------------------------------------------------------------------------*/
	private Timer inputListeningTimer;
	private void startInputListeningTimer() {
		final int LISTENING_DELAY = 75;

		TimerTask timerTask = new TimerTask(){
			public void run() {
				//check if inputStream has messages in it
				if( !inputStreamIsReady() || inputListeningIsPaused) {
					return;
				}

				//while there is still messages in stream, keep placing them in queue.
				while ( inputStreamIsReady() ) {
					String message;
					if( (message = getNextStreamMessage()) != null ) {
                        try {
                            notifyListenersOfChatNetMessageReceived(message);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
					return;
				}
			}
		};
		System.out.println("TIMER STARTED: inputTimer (socket inbound)");
		
		// running timer task as daemon thread
		inputListeningTimer = new Timer(true);
		inputListeningTimer.scheduleAtFixedRate(timerTask, 0, LISTENING_DELAY);
	}

	//this method sets up a task that runs every <LISTENING_DELAY> milliseconds
	//every time the task runs, it empties out the queue by every item through the outputStream
	private Timer outputListeningTimer;
	private void startOutputListeningTimer() {
		final long LISTENING_DELAY = 125L;

		TimerTask timerTask = new TimerTask(){
			public void run() {
				if ( outputMessageQueue.size() > 0 ) {
					String nextMessage = outputMessageQueue.poll();
					// System.out.println("TIME NOW:" + System.currentTimeMillis() + " Sending:" + nextMessage);
					socketOutput.println(nextMessage);
				}
			}
		};

		System.out.println("TIMER STARTED: outputTimer (socket outbound)");
		// running timer task as daemon thread
		outputListeningTimer = new Timer(true);
		outputListeningTimer.scheduleAtFixedRate(timerTask, LISTENING_DELAY, LISTENING_DELAY);
	}
	
	//this timer checks whether connection is still alive
	private Timer timeoutTimer;
	private void timeoutTimer() {
		final int TIMEOUT_DELAY = 45000;

		TimerTask timerTask = new TimerTask(){
			public void run() {
				socketOutput.println("NOOP");
				if (socketOutput.checkError()) {
					System.out.println("TimeoutTimer: ERROR - SAVING DATA");
                    try {
                        StatsSerialization.saveStats(BotGlobal.playerPubDuelRatings);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.out.println("TimeoutTimer: ERROR - CLOSING SOCKET");
					BotGlobal.setConnected(false);
					try { 
						socketOutput.close();
						socketInput.close();
						rawSocket.close();
						
					} catch (IOException e) { e.printStackTrace(); }
				}
				return;
			}
		};

		System.out.println("TIMER STARTED: timeoutTimer (keepalive)");
		// running timer task as daemon thread
		timeoutTimer = new Timer(true);
		timeoutTimer.scheduleAtFixedRate(timerTask, 0, TIMEOUT_DELAY);
	}
	
}



