package bot;

import bot.statparser.Player;
import bot.statparser.TableExtract;

import java.io.IOException;
import java.util.HashSet;


/**
 * Sample main function used to setup socket and socket listener classes.
 *
 * @author Alex
 */
public class BotMain {

	private static final String HOCKEYZONE_IP = "162.248.95.143";
	public ChatnetSocket chatnetSocket;

	public static void main(String args[]) throws IOException {
		try {
			BotGlobal.setup();
		} catch (Exception e) {		}

		while (true) {
			if (!BotGlobal.isConnected()) {
				System.out.println("connecting...");
				try {
					BotGlobal.players = TableExtract.getPlayerData(BotGlobal.players);
					activateBot(BotGlobal.players);
				} catch (InterruptedException ex) {
					System.out.println("stats parsing error!");
				}
			}
		}
	}

	private static void activateBot(HashSet<Player> players) throws IOException {
		// Instantiate ChatnetSocket
		// MAIN BOT SOCKET
		ChatnetSocket pubBotSocket = new ChatnetSocket();
		// OBSERVATION SOCKETS
		ChatnetSocket scrimObservationSocket = new ChatnetSocket();

		// Instantiate and setup listener and controller class

		// RankingUtils.orderPubDuelRanking();
		// RankingUtils.orderPubRanking();

		// pubBotSocket.addListener(BotGlobal.publicArenaController);
		scrimObservationSocket.addListener(BotGlobal.scrimArenaController);

		/*
		 * Connect ChatnetSocket Connect method is basically
		 * chatnetSocket.connect("IP address", port#, "username", "password",
		 * "briefDescription") ;
		 */
		// pubBotSocket.connect(HOCKEYZONE_IP, 7501, "bot of bot", "furbot", "furStatBot");
		scrimObservationSocket.connect(HOCKEYZONE_IP, 7501, "bot of scrim", "furbot", "furStatBot");

		// Go to pub
		// pubBotSocket.send("GO:");
		// pubBotSocket.send("SEND:PUB:?game 0");

		scrimObservationSocket.send("GO:");
		// scrimObservationSocket.send("SEND:PUB:?game 3");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		 scrimObservationSocket.send("SEND:PUB:?game 3");
		// scrimObservationSocket.send("SEND:PUB:observing scrim arena...");

		// Now a loop to keep program alive while connection is alive. When
		// socket is closed
		// int i = -30;

		while (scrimObservationSocket.isConnected()) {
			try {
				Thread.sleep(500);
				// i++;
			} catch (InterruptedException e) {
				System.out.println("--- BOT MAIN - ERROR");
			//	StatsSerialization.saveStats(BotGlobal.playerPubDuelRatings);
				e.printStackTrace();
			}

			// if (i % 600 == 0) {
			// System.out.println("updating stats...");
			// backend.updateStats();
			// i = 1;
		}
	}
}