package bot;

import bot.duel.Duel;
import bot.duel.EloCalc;
import bot.duel.PlayerDuelStats;
import bot.duel.PubPowerPlayer;
import bot.duel.StatsSerialization;
import bot.duel.ranking.RankingUtils;
import bot.statparser.Player;
import bot.statparser.TableExtract;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The controller implements the ChatnetSocketListener and deals with the logic
 * for user commands.
 *
 * @author Fur - framework by Nax
 */
public class PublicArenaController implements ChatnetSocketListener {

	private static final String OWNED = "You OWNED ";
	private static final String DEFEATED = "DEFEATED by ";
	private static final String CAS_DUEL_NEW_RANK = " in a casual duel! (NEW POWER RANKING:";
	private static final String INVALID_DUEL = "Your duel was classed as invalid - perhaps you or your designated partner did not play.";

	public int replies = 0;

	ChatnetUtils utils = new ChatnetUtils();
	String BOT_ADMIN = "fur of fur";
	private ChatnetSocket socket;

	private HashSet<Player> players;

	private HashSet<String> arenaPlayerList;
	private HashSet<String> pubPlayerList;
	// private List<String> miniPlayerList;
	// private List<String> scrimPlayerList;
	private HashSet<String> duelPlayerList;
	private HashSet<PubPowerPlayer> pubPowerPlayers;

	// public LinkedList<PlayerDuelStats> playerRatingList;

	private HashSet<Duel> activeDuels;
	private Boolean duelsOnline = false;

	private int botStatRequests = 0;
	private int duelsServed = 0;

	PublicArenaController(HashSet<Player> players2) {
		this.players = players2;
		System.out.println("PLAYERS IN DB:" + players2.size());

		arenaPlayerList = new HashSet<>();
		pubPlayerList = new HashSet<>();
		activeDuels = new HashSet<>();
		pubPowerPlayers = new HashSet<>();
	}

	private void updateStats() throws IOException {
		try {
			duelsOnline = false;

			players.clear();
			players = TableExtract.getPlayerData(players);

			duelPlayerList = pubPlayerList;

			if (!pubPowerPlayers.isEmpty()) {
				updatePubPowerRankings();
			}

			if (!activeDuels.isEmpty()) {
				verifyDuels();
				activeDuels.clear();
			}

			if (pubPowerPlayers.size() >= 8) {
				duelsOnline = true;
				socket.send("SEND:PUB:PM me !duel - climb the pub dueling ladder, be the best.");
                System.out.println("CASUAL DUELS NOW AVAILABLE");
			} else {
			    duelsOnline = false;
                socket.send("SEND:PUB:pub dueling ladder offline - due to a lack of players.");
            }

		} catch (InterruptedException ex) {
			Logger.getLogger(PublicArenaController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void processChatnetMessage(ChatnetSocket socket, String message, String source) throws IOException {

		if (this.socket == null) {
			this.socket = socket;
		}

		// DEBUG
		// System.out.println("PROCESSING:" + source + ":HZ:" + message);

		/*
		 * utils.chatnetTokenize() splits chatnet messages by their different
		 * sections delimited by colon characters Refer to:
		 * http://wiki.minegoboom.com/index.php/ChatNet_Protocol
		 */
		String[] tokens = utils.chatnetTokenize(message);

		if(tokens != null) {

			// Checks the tokens to see if the method for updating all public
			// rankings should be called.
			// Should be called by a public arena observing source
			validatePubRankingUpdate(tokens);

			// Adds players to the global duel ranking list (actually tracks all
			// players that are in HZ public arena when the bot joins)
			// Should be called by a public or global arena observing source
			validateArenaEntree(tokens);

			// Adds players to the global duel ranking list (actually tracks all
			// players that are enter Z public arena after the bot)
			// Should be called by a public or global arena observing source
			validateNewArenaEntree(tokens);

			// monitors which players are joining or leaving freq 0 and 1, updates
			// duelling and 6 on 6 tracking lists.)
			// Should be called by a public observing source
			updateGameZeroPlayers(tokens);

			// If message is a !command pm'ed to this bot
			processUserCommand(socket, tokens);
		}
	}

	private void processUserCommand(ChatnetSocket socket, String[] tokens) throws IOException {
		if (tokens[0].equals("MSG") && tokens[1].equals("PRIV") && tokens[3].startsWith("!")) {

			String targetPlayer = tokens[2];
			String command = tokens[3].toLowerCase();

			System.out.println("PARSING COMMAND:" + command);

			for (PlayerDuelStats player : BotGlobal.playerPubDuelRatings) {
				if (command.contains(" " + player.getName().toLowerCase())) {
					targetPlayer = player.getName();
					System.out.println("TARGET PLAYER IS NOW:" + targetPlayer);
				}
			}

			command = command.toLowerCase();
			targetPlayer = targetPlayer.toLowerCase();
			command = command.replace(targetPlayer, "");
			command = command.trim();

			System.out.println("PASSING COMMAND-->" + command + " :TARGET PLAYER -->" + targetPlayer);

			switch (command) {
			case "!help":
				socket.send("SEND:PRIV:" + tokens[2] + ":"
						+ "Rating Commands: !top + goals / assists / shot% / save% / pucktime / checks / impact (+/-) / steals / bowls");
				 
				socket.send("SEND:PRIV:" + tokens[2] + ":"
						+ "Other Commands: !top elos (NEW) / !top duelists (NEW) / !elo (YOUR RANKINGS) / !duel (AVAILABLE IN PUBLIC FULLCOURT) / !usage / !owner");
				break;

			case "!owner":
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "The owner of this bot is..." + BOT_ADMIN);
				break;

			case "!usage":
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "A total of " + botStatRequests
						+ " stats have been served during this session");
				 
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "A total of " + duelsServed
						+ " duels have been completed during this session");
				break;

			case "!duel":
				if (duelsOnline) {
					int duelInt = duelPlayerList.size();

					System.out.println("CHECKING EXISTING DUELS...");

					for (Duel duel : activeDuels) {
						if (duel.name_home.equalsIgnoreCase(tokens[2])) {
							socket.send("SEND:PRIV:" + tokens[2] + ":" + "You are currently dueling " + duel.name_away);
							return;
						}

						if (duel.name_away.equalsIgnoreCase(tokens[2])) {
							socket.send("SEND:PRIV:" + tokens[2] + ":" + "You are currently dueling " + duel.name_home);
							return;
						}
					}

					System.out.println("CHECKING FOR OPPONENTS...");
					if (duelPlayerList.size() < 2) {
						socket.send("SEND:PRIV:" + tokens[2] + ":" + "No opponent available at this time...");
						return;
					}

					System.out.println("CREATING NEW DUEL...");
					Random rand = new Random();
					int randomNumber = rand.nextInt(duelInt);
					String target = "";

					List<String> duelists = new LinkedList<>();
					duelists.addAll(duelPlayerList);

					while (tokens[2].equalsIgnoreCase(duelists.get(randomNumber))) {
						randomNumber = rand.nextInt(duelInt);
					}

					target = duelists.get(randomNumber);

					duelPlayerList.remove(tokens[2]);
					duelPlayerList.remove(target);

					Duel duel = new Duel();
					duel.name_home = tokens[2];
					duel.name_away = target;

					for (Player x : players) {
						if (x.name.equalsIgnoreCase(tokens[2])) {
							duel.start_rating_home = x.rating;
						}

						if (x.name.equalsIgnoreCase(target)) {
							duel.start_rating_away = x.rating;
						}
					}

					System.out.println(tokens[2] + " : " + duel.start_rating_home);
					System.out.println(target + " : " + duel.start_rating_away);

					activeDuels.add(duel);

					socket.send("SEND:PRIV:" + tokens[2] + ":" + "Notifying " + target
							+ " they have been challenged to a casual duel.");
					 
					socket.send("SEND:PRIV:" + target + ":" + "You have been entered into a casual duel with "
							+ tokens[2] + ", good luck...");
					break;
				} else {
					socket.send("SEND:PRIV:" + tokens[2] + ":"
							+ "DUELS ARE AVAILABLE WHEN A NEW GAME OF FULLCOURT PUBLIC STARTS...");
					 
				}
				break;

			case "!top goals":
				List<Player> resultList = TableExtract.getTopGoals(players);
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "Showing Public Top Goal Scorers");
				int i = 0;
				try {
					for (Player player : resultList) {
						String reply = ++i + ") " + "Name: " + player.name + " --> " + player.goals;
						socket.send("SEND:PRIV:" + tokens[2] + ":" + reply);

						 
					}
				} catch (Exception e) {
					System.out.println(e);
				}
				botStatRequests++;
				break;

			case "!top assists":
				List<Player> resultList2 = TableExtract.getTopAssists(players);
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "Showing Public Top Assists");
				int j = 0;
				try {
					for (Player player : resultList2) {
						String reply = ++j + ") " + "Name: " + player.name + " --> " + player.assists;
						socket.send("SEND:PRIV:" + tokens[2] + ":" + reply);
					}
				} catch (Exception e) {
					System.out.println(e);
				}
				botStatRequests++;
				break;

			case "!top save%":
				List<Player> resultList3 = TableExtract.getTopSavePcnt(players);
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "Showing Public Top Save Percentages");
				int k = 0;

				try {
					for (Player player : resultList3) {
						String reply = ++k + ") " + "Name: " + player.name + " --> " + player.savePcnt + "%";
						socket.send("SEND:PRIV:" + tokens[2] + ":" + reply);
					}
				} catch (Exception e) {
					System.out.println(e);
				}

				botStatRequests++;
				break;

			case "!top shot%":
				List<Player> resultList4 = TableExtract.getTopShotAcc(players);
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "Showing Public Top Shot Accuracy Percentages");
				int l = 0;
				for (Player player : resultList4) {
					String reply = ++l + ") " + "Name: " + player.name + " --> " + player.shotAcc + "% (" + player.goals
							+ "g)";
					socket.send("SEND:PRIV:" + tokens[2] + ":" + reply);
				}
				botStatRequests++;
				break;

			case "!top hog":
			case "!top pucktime":
				List<Player> resultList5 = TableExtract.getTopPuckTime(players);
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "Showing Public Top Hogs");
				int m = 0;
				try {
					for (Player player : resultList5) {
						String reply = ++m + ") " + "Name: " + player.name + " --> " + player.puckTimeSeconds
								+ " seconds (" + player.puckControls + "pc)";
						socket.send("SEND:PRIV:" + tokens[2] + ":" + reply);
					}
				} catch (Exception e) {
					System.out.println(e);
				}
				botStatRequests++;
				break;
			// new...
			case "!top checks":
				List<Player> resultList6 = TableExtract.getTopChecks(players);
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "Showing Public Top Checkers");
				int i6 = 0;
				try {
					for (Player player : resultList6) {
						String reply = ++i6 + ") " + "Name: " + player.name + " --> " + player.checks;
						socket.send("SEND:PRIV:" + tokens[2] + ":" + reply);
					}
				} catch (Exception e) {
					System.out.println(e);
				}
				botStatRequests++;
				break;

			case "!top impact":
				List<Player> resultList7 = TableExtract.getTopImpact(players);
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "Showing Public Top +/-");
				int i7 = 0;
				try {
					for (Player player : resultList7) {
						String reply = ++i7 + ") " + "Name: " + player.name + " --> " + player.goalDif;
						socket.send("SEND:PRIV:" + tokens[2] + ":" + reply);
					}
				} catch (Exception e) {
					System.out.println(e);
				}
				botStatRequests++;
				break;

			case "!top steals":
				List<Player> resultList8 = TableExtract.getTopSteals(players);
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "Showing Public Top Steals");
				int i8 = 0;
				try {
					for (Player player : resultList8) {
						String reply = ++i8 + ") " + "Name: " + player.name + " --> " + player.steals
								+ "  (avg. per game:" + player.stealsPerPeriod + ")";
						socket.send("SEND:PRIV:" + tokens[2] + ":" + reply);
					}
				} catch (Exception e) {
					System.out.println(e);
				}
				botStatRequests++;
				break;

			// end new...
			case "!top bowls":
				socket.send("SEND:PRIV:" + tokens[2] + ":" + "1) Koroshi");
				botStatRequests++;
				break;

			case "!elo":
			case "!powerrank":
				for (PlayerDuelStats player : BotGlobal.playerPubDuelRatings) {
					if (player.getName().equalsIgnoreCase(targetPlayer)) {
						socket.send("SEND:PRIV:" + tokens[2] + ":" + targetPlayer + " DUEL POWER RANKING:"
								+ player.getElo());
						socket.send("SEND:PRIV:" + tokens[2] + ":" + targetPlayer + " PUBLIC POWER RANKING:"
								+ player.getPubElo());
						botStatRequests++;
					}
				}
				break;

			// ADMIN / SECRET
			case "!list elo":
				if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
					for (PlayerDuelStats player : BotGlobal.playerPubDuelRatings) {
						System.out.println(player.getName() + " -> Power Ranking: " + player.getElo());
						if (player.getPubElo() == 0.0) {
							player.setPubElo(5000.0);
						}
						System.out.println(player.getName() + " -> Power Ranking: " + player.getPubElo());
					}
					System.out.println("Total Players:" + BotGlobal.playerPubDuelRatings.size());
				}
				break;

			case "!top duelists":
				List<String> resultList9 = RankingUtils.orderPubDuelRanking(tokens[2]);
				for (String response : resultList9) {
					socket.send("SEND:PRIV:" + tokens[2] + ":" + response);
					 
				}
				break;

			case "!top elos":
				List<String> resultList10 = RankingUtils.orderPubRanking(tokens[2]);
				for (String response : resultList10) {
					socket.send("SEND:PRIV:" + tokens[2] + ":" + response);
				}
				break;

			case "!save elo":
				if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
					System.out.println("--- SAVING SER FILE ---");
					StatsSerialization.saveStats(BotGlobal.playerPubDuelRatings);
				}
				break;

			case "!load elo":
				if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
					System.out.println("--- LOADING SER FILE ---");
					BotGlobal.playerPubDuelRatings = StatsSerialization.loadStats();
				}
				break;

			case "!shutdown":
				// You can make administrator commands like this, you check
				// that
				// user that pm's you with admin command is allowed
				if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
					socket.send("SEND:PRIV:" + tokens[2] + ":" + "Bot is now shutting down...");
					StatsSerialization.saveStats(BotGlobal.playerPubDuelRatings);
					// Give it a second to send message through socket
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					// Close the socket
					socket.close();
				}
				break;
			}
		}
	}

	private void updateGameZeroPlayers(String[] tokens) {
		if (tokens[0].equals("SHIPFREQCHANGE")) {
			if (tokens[3].equals("0") || tokens[3].equals("1")) {
				System.out.println(tokens[1] + " MOVED TO OR SHIPCHANGED ON A PUBLIC FREQ");
				if (!pubPlayerList.contains(tokens[1])) {
					pubPlayerList.add(tokens[1]);
					System.out.println("... AND WAS ADDED TO TO PUB LIST - SIZE NOW:" + pubPlayerList.size());

					for (Player x : players) {
						if (x.name.equalsIgnoreCase(tokens[1])) {
							PubPowerPlayer player = new PubPowerPlayer(tokens[1], x.getGoalDif(), 0,
									Integer.valueOf(tokens[3]), false);

							pubPowerPlayers.add(player);
							System.out.println(
									"... AND WAS ADDED TO TO PUB POWER LIST - SIZE NOW:" + pubPowerPlayers.size());
						}
					}
				} else {
					System.out.println("... AND WAS ALREADY ON THE PUB LIST - SIZE NOW:" + pubPlayerList.size());
					// UPDATE FREQ DETAILS...
					for (PubPowerPlayer player : pubPowerPlayers) {
						if (player.name.equalsIgnoreCase(tokens[1])) {
							player.frequency = Integer.valueOf(tokens[3]);
						}
					}
				}
			} else {
				System.out.println(tokens[1] + " WENT SOMEWHERE THAT ISNT PUBLIC");
				playerLeaving(tokens[1]);
			}
		}

		if (tokens[0].equals("LEAVING")) {
			System.out.println(tokens[1] + " DETECTED LEAVING ARENA");
			arenaPlayerList.remove(tokens[1]);

			playerLeaving(tokens[1]);
		}
	}

	private void playerLeaving(String playerName) {
		if (pubPlayerList.contains(playerName)) {
            pubPlayerList.remove(playerName);
            System.out.println("... AND WAS REMOVED FROM THE PUB LIST - SIZE NOW:" + pubPlayerList.size());

            PubPowerPlayer targetPowerPlayer = null;
            for (PubPowerPlayer powerPlayer : pubPowerPlayers) {
                if (powerPlayer.name.equalsIgnoreCase(playerName)) {
                    targetPowerPlayer = powerPlayer;
                }
            }
            if (targetPowerPlayer != null) {
                pubPowerPlayers.remove(targetPowerPlayer);
                System.out.println("... AND WAS REMOVED FROM PUB POWER LIST - SIZE NOW:" + pubPowerPlayers.size());
            }
        }
	}

	private void validateNewArenaEntree(String[] tokens) {
		if (tokens[0].equals("ENTERING")) {
			arenaPlayerList.add(tokens[1]);
			System.out.println(tokens[1] + " DETECTED IN ARENA");

			boolean newPlayer = true;
			for (PlayerDuelStats player : BotGlobal.playerPubDuelRatings) {
				if (player.name.equalsIgnoreCase(tokens[1])) {
					newPlayer = false;
				}
			}

			if (newPlayer) {
				PlayerDuelStats nextPlayer = new PlayerDuelStats(tokens[1]);
				BotGlobal.playerPubDuelRatings.add(nextPlayer);
			}
		}
	}

	private void validateArenaEntree(String[] tokens) {
		if (tokens[0].equals("PLAYER")) {
			arenaPlayerList.add(tokens[1]);
			System.out.println(tokens[1] + " DETECTED IN ARENA");
			// System.out.println("TOKENS LENGTH:" + tokens.length);

			boolean newPlayer = true;
			PlayerDuelStats playerStats = null;
			for (PlayerDuelStats player : BotGlobal.playerPubDuelRatings) {
				if (player.name.equalsIgnoreCase(tokens[1])) {
					newPlayer = false;
					playerStats = player;
				}
			}

			if (newPlayer) {
				PlayerDuelStats nextPlayer = new PlayerDuelStats(tokens[1]);
				playerStats = nextPlayer;
				BotGlobal.playerPubDuelRatings.add(nextPlayer);
			}

			if (tokens[3].equals("0") || tokens[3].equals("1")) {
				System.out.println(tokens[1] + " FOUND ON A PUBLIC FREQ");
				if (!pubPlayerList.contains(tokens[1])) {
					pubPlayerList.add(tokens[1]);
					System.out.println("... AND WAS ADDED TO TO PUB LIST - SIZE NOW:" + pubPlayerList.size());

					boolean addedToPower = false;
					for (Player x : players) {
						if (x.name.equalsIgnoreCase(tokens[1])) {
							PubPowerPlayer player = new PubPowerPlayer(tokens[1], x.getGoalDif(), 0,
									Integer.valueOf(tokens[3]), playerStats.ignoreMe);

							pubPowerPlayers.add(player);
							addedToPower = true;
							System.out.println(
									"... AND WAS ADDED TO TO PUB POWER LIST - SIZE NOW:" + pubPowerPlayers.size());
						}
					}

					if(!addedToPower){
						System.out.println("COULD NOT EASILY ADD TO POWER:" + tokens[1]);

						PubPowerPlayer player = new PubPowerPlayer(tokens[1], 0, 0,
																   Integer.valueOf(tokens[3]), false);

						pubPowerPlayers.add(player);

					}
				}
			}
		}
	}

	private void validatePubRankingUpdate(String[] tokens) throws IOException {
		if (tokens[0].equals("MSG") && tokens[1].equals("ARENA")) {
			if (tokens[2].equals("* GAME 0 TOTAL STATS")) {
				// DETECT THIS IS FROM PUB 0...???
				updateStats();
			}
		}
	}

	@Override
	public void chatnetSocketHasClosed(ChatnetSocket socket) {
		// TODO Auto-generated method stub
	}

	// EXPERIMENTAL CODE.
	private void updatePubPowerRankings() throws IOException {
		List<String> uniquePowerPlayers = new LinkedList<>();

		for (PubPowerPlayer powerPlayer : pubPowerPlayers) {
			boolean match = false;
			for (String powerPlayerName : uniquePowerPlayers) {
				if (powerPlayerName.equalsIgnoreCase(powerPlayer.name)) {
					powerPlayer.removeFlag = true;
					match = true;
				}
			}
			if (!match) {
				uniquePowerPlayers.add(powerPlayer.name);
			}
		}

		int totalPlayers = pubPowerPlayers.size();
		System.out.println("TOTAL PLAYERS:" + totalPlayers);

		double totalRankingFreqZero = 0;
		double freqZeroCount = 0;
		double totalRankingFreqOne = 0;
		double freqOneCount = 0;
		double totalRanking = 0;

		for (PubPowerPlayer powerPlayer : pubPowerPlayers) {
			for (PlayerDuelStats playerStat : BotGlobal.playerPubDuelRatings) {
				if (powerPlayer.name.equalsIgnoreCase(playerStat.getName())) {
					powerPlayer.pubPowerRank = playerStat.getPubElo();
					powerPlayer.ignoreMe = playerStat.ignoreMe;
					totalRanking = totalRanking + playerStat.getPubElo();

					if (powerPlayer.frequency == 0) {
						totalRankingFreqZero = totalRankingFreqZero + playerStat.getPubElo();
						freqZeroCount++;
					} else if (powerPlayer.frequency == 1) {
						totalRankingFreqOne = totalRankingFreqOne + playerStat.getPubElo();
						freqOneCount++;
					}
				}
			}
		}

		// CALCULATE AVERAGE ON THE ICE RANKING.
		double arenaAvgElo = totalRanking / totalPlayers;
		System.out.println("AVERAGE RANKING OF ALL PLAYERS ON THE ICE:" + arenaAvgElo);

		double freqZeroAvgElo = totalRankingFreqZero / freqZeroCount;
		System.out.println("AVERAGE RANKING OF FREQ ZERO:" + freqZeroAvgElo);

		double freqOneAvgElo = totalRankingFreqOne / freqOneCount;
		System.out.println("AVERAGE RANKING OF FREQ ONE:" + freqOneAvgElo);

		if (freqZeroAvgElo > freqOneAvgElo) {
			socket.send("SEND:PUB:FREQ ZERO WERE EXPECTED TO WIN --> RANKINGS: FREQ 0 (" + freqZeroAvgElo
					+ ") VS FREQ 1 (" + freqOneAvgElo + ")");
		} else {
			socket.send("SEND:PUB:FREQ ONE WERE EXPECTED TO WIN --> RANKINGS: FREQ 0 (" + freqZeroAvgElo
					+ ") VS FREQ 1 (" + freqOneAvgElo + ")");
		}

		// CALCULATE GOAL DIFFERENCES
		System.out.println("POWER PLAYERS:" + pubPowerPlayers.size());
		for (PubPowerPlayer powerPlayer : pubPowerPlayers) {
			System.out.println("powerPlayer:" + powerPlayer.name + " --> FREQ:" + powerPlayer.frequency);
			for (Player player : players) {
				if (powerPlayer.name.equalsIgnoreCase(player.name)) {
					powerPlayer.final_goalDiff = player.goalDif;
				}
			}
		}

		// UPDATE SCORES...
        final int MIN_FC_PLAYERS = 8;

		for (PubPowerPlayer powerPlayer : pubPowerPlayers) {
			for (PlayerDuelStats playerStat : BotGlobal.playerPubDuelRatings) {
				if (powerPlayer.name.equalsIgnoreCase(playerStat.getName())) {
					if (powerPlayer.frequency == 0) {
						if (pubPowerPlayers.size() >= MIN_FC_PLAYERS) {
							playerStat.setPubElo(EloCalc.calculateNewTeamBasedElo(playerStat.getPubElo(), freqOneAvgElo,
									(powerPlayer.final_goalDiff - powerPlayer.start_goalDiff)));
						} else {
							playerStat.setPubElo(playerStat.getPubElo() + 3);
						}
					} else if (powerPlayer.frequency == 1) {
						if (pubPowerPlayers.size() >= MIN_FC_PLAYERS) {
							playerStat.setPubElo(EloCalc.calculateNewTeamBasedElo(playerStat.getPubElo(),
									freqZeroAvgElo, (powerPlayer.final_goalDiff - powerPlayer.start_goalDiff)));
						} else {
							playerStat.setPubElo(playerStat.getPubElo() + 3);
						}
					}
					// UPDATE PLAYER
					if (pubPowerPlayers.size() >= MIN_FC_PLAYERS) {
						socket.sendNoListen("SEND:PRIV:" + powerPlayer.name + ":"
								+ "YOUR NEW PUBLIC POWER RANKING SCORE:" + playerStat.getPubElo());
						System.out.println("UPDATED:" + powerPlayer.name + " --> NEW RANK:" + playerStat.getPubElo());
						 
					} else {
						socket.sendNoListen("SEND:PRIV:" + powerPlayer.name + ":"
								+ "THANKYOU FOR PLAYING HZ - NEW RANK:" + playerStat.getPubElo());
						System.out.println("REWARDED:" + powerPlayer.name + " --> NEW RANK:" + playerStat.getPubElo());
						 
					}

					// UPDATE FOR NEXT ROUND...
					powerPlayer.start_goalDiff = powerPlayer.final_goalDiff;
					powerPlayer.pubPowerRank = playerStat.getPubElo();
				}
			}
		}

		// SAVE ANY CHANGES
		StatsSerialization.saveStats(BotGlobal.playerPubDuelRatings);
	}

	private void verifyDuels() throws IOException {
        List<String> winners = new LinkedList<>();

        if(winners.size()!=0) {
            winners.clear();
        }

		for (Duel x : activeDuels) {
			double homeRating = 0;
			double awayRating = 0;

			for (Player y : players) {
				if (y.name.equalsIgnoreCase(x.name_home)) {
					x.final_rating_home = y.rating;
					homeRating = x.final_rating_home - x.start_rating_home;
					System.out.println(x.name_home + " rating change: " + (homeRating));
				}

				if (y.name.equalsIgnoreCase(x.name_away)) {
					x.final_rating_away = y.rating;
					awayRating = x.final_rating_away - x.start_rating_away;
					System.out.println(x.name_away + " rating change: " + (awayRating));
				}
			}

			PlayerDuelStats homePlayerTemp = new PlayerDuelStats();
			PlayerDuelStats awayPlayerTemp = new PlayerDuelStats();
			double newHomeElo = 0.0;
			double newAwayElo = 0.0;

			for (PlayerDuelStats player : BotGlobal.playerPubDuelRatings) {
				if (player.name.equalsIgnoreCase(x.name_home)) {
					homePlayerTemp = player;
				}
				if (player.name.equalsIgnoreCase(x.name_away)) {
					awayPlayerTemp = player;
				}
			}

			if (homeRating != 0 && awayRating != 0) {
				if (homeRating >= awayRating) {
					// HOME WIN
					for (PlayerDuelStats player : BotGlobal.playerPubDuelRatings) {
						if (player.name.equalsIgnoreCase(x.name_home)) {
							player.setElo(
									EloCalc.calculateNewTeamElo(homePlayerTemp.getElo(), awayPlayerTemp.getElo(), 1));
							newHomeElo = player.getElo();
						}
						if (player.name.equalsIgnoreCase(x.name_away)) {
							player.setElo(
									EloCalc.calculateNewTeamElo(awayPlayerTemp.getElo(), homePlayerTemp.getElo(), -1));
							newAwayElo = player.getElo();
						}
					}

					socket.sendNoListen("SEND:PRIV:" + x.name_home + ":" + OWNED + x.name_away + CAS_DUEL_NEW_RANK
							+ newHomeElo + ")");
					winners.add(x.name_home);
					socket.sendNoListen("SEND:PRIV:" + x.name_away + ":" + DEFEATED + x.name_home + CAS_DUEL_NEW_RANK
							+ newAwayElo + ")");
				} else {
					// AWAY WIN
					for (PlayerDuelStats player : BotGlobal.playerPubDuelRatings) {
						if (player.name.equalsIgnoreCase(x.name_home)) {
							player.setElo(
									EloCalc.calculateNewTeamElo(homePlayerTemp.getElo(), awayPlayerTemp.getElo(), -1));
							newHomeElo = player.getElo();
						}
						if (player.name.equalsIgnoreCase(x.name_away)) {
							player.setElo(
									EloCalc.calculateNewTeamElo(awayPlayerTemp.getElo(), homePlayerTemp.getElo(), 1));
							newAwayElo = player.getElo();
						}
					}
					socket.sendNoListen("SEND:PRIV:" + x.name_away + ":" + OWNED + x.name_home + CAS_DUEL_NEW_RANK
							+ newAwayElo + ")");
					winners.add(x.name_away);
					socket.sendNoListen("SEND:PRIV:" + x.name_home + ":" + DEFEATED + x.name_away + CAS_DUEL_NEW_RANK
							+ newHomeElo + ")");
				}
			} else {
				System.out.println("DUEL RESULT: VOID");
				socket.sendNoListen("SEND:PRIV:" + x.name_away + ":" + INVALID_DUEL);
				socket.sendNoListen("SEND:PRIV:" + x.name_home + ":" + INVALID_DUEL);
			}
			duelsServed++;
		}

		for(String winner:winners){
            socket.sendNoListen("SEND:PUB:A new champion has arisen: " + winner + " has just won a casual rating duel in arena 0!");
        }
		// SAVE ANY CHANGES
		StatsSerialization.saveStats(BotGlobal.playerPubDuelRatings);
	}
}