package bot.duel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.LinkedList;

import org.json.simple.JSONObject;

public class StatsSerialization {

	public static void saveStats(HashSet<PlayerDuelStats> duelStats) throws IOException {

		LinkedList<PlayerDuelStats> duelStatsSave = new LinkedList<>();

		duelStatsSave.addAll(duelStats);
		// write object to file
		FileOutputStream fos = new FileOutputStream("pubduelstats.ser");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(duelStatsSave);
		oos.close();

		 //savePlayerJSONs(duelStats);
	}

//	@SuppressWarnings("unchecked")
//	private static void savePlayerJSONs(HashSet<PlayerDuelStats> duelStats) {
//		System.out.println("--- SAVING JSONS ---" + "  PLAYER TOTAL:" + duelStats.size());
//		for (PlayerDuelStats player : duelStats) {
//
//			String fileNameCleaned = player.name.replaceAll("[^\\w.-]", "_");
//			fileNameCleaned = fileNameCleaned.toUpperCase();
//
//			File file = new File("D://workspace//HzBot//JSON//" + fileNameCleaned + player.name.toUpperCase().hashCode() + ".json");
//			if (!file.exists()) {
//				try {
//					file.createNewFile();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//
//			try (FileWriter saveTarget = new FileWriter("D://workspace//HzBot//JSON//" + fileNameCleaned + player.name.toUpperCase().hashCode() + ".json")) {
//				JSONObject playerObj = new JSONObject();
//				playerObj.put("duelElo", player.duelElo);
//				playerObj.put("pubElo", player.pubElo);
//				playerObj.put("name", player.name);
//
//				saveTarget.write(playerObj.toJSONString());
//				saveTarget.flush();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//	}

	@SuppressWarnings("unchecked")
	public static HashSet<PlayerDuelStats> loadStats() {
		LinkedList<PlayerDuelStats> duelStats;
		HashSet<PlayerDuelStats> duelStatsSet = new HashSet<>();

		try {
			// read object from file
			FileInputStream fis = new FileInputStream("pubduelstats.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			duelStats = (LinkedList<PlayerDuelStats>) ois.readObject();
			ois.close();

			for (PlayerDuelStats player : duelStats) {
				System.out.println("NAME:" + player.getName() + ", DUELRANK:" + player.getElo());
				System.out.println("NAME:" + player.getName() + ", PUBRANK:" + player.getPubElo());
				System.out.println("--- --- ---");
				duelStatsSet.add(player);
			}

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return duelStatsSet;
	}

}
