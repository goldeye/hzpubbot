package bot.duel;

/**
 *
 * @author Alex
 */
public class PubPowerPlayer {
    public String name;
    public double pubPowerRank; 
    public int start_goalDiff;
    public int final_goalDiff;
    public boolean removeFlag;
    public boolean ignoreMe;
    public int frequency;
    
    public PubPowerPlayer(String name, int sgd, int fgd, int freq, boolean ignoreMe){
    	this.name = name;
    	this.start_goalDiff = sgd;
    	this.final_goalDiff = fgd;
    	this.frequency = freq;
    	this.ignoreMe = ignoreMe;
    }
}
