package bot.duel;

public class EloCalc {
	
	// for stand alone testing at this time.
    public static void main(String[] args) throws InterruptedException {

    	System.out.println(calculateNewTeamElo(20000, 20000, 1));
		System.out.println(calculateNewTeamElo(20000, 20000, 0));
    	System.out.println(calculateNewTeamElo(20000, 20000, -1));
    	
    	System.out.println(calculateNewTeamBasedElo(10000, 5000, 1));
    	System.out.println(calculateNewTeamBasedElo(10000, 5000, 2));
    	System.out.println(calculateNewTeamBasedElo(10000, 5000, 3));
    	System.out.println(calculateNewTeamBasedElo(10000, 5000, 4));
    	System.out.println(calculateNewTeamBasedElo(10000, 5000, 5));
    	System.out.println(calculateNewTeamBasedElo(10000, 5000, 6));
    	System.out.println(calculateNewTeamBasedElo(10000, 5000, 7));
    	System.out.println(calculateNewTeamBasedElo(5000, 5000, 8));
    	System.out.println(calculateNewTeamBasedElo(5000, 5000, 9));
    	System.out.println(calculateNewTeamBasedElo(5000, 5000, 10));

		System.out.println(calculateNewTeamBasedElo(5000, 5000, -10));
		System.out.println(calculateNewTeamBasedElo(5000, 5000, -9));
		System.out.println(calculateNewTeamBasedElo(5000, 5000, -8));

    }

	public EloCalc() {	}

	public static double calculateNewTeamElo(double targetTeamElo, double opponentElo, int result) {
		int newElo = 0;
		double scoreMod;
		int K = 320;
		
		// win
		if (result == 1) {
			scoreMod = 1.0;
		}

		// draw
		else if (result == 0) {
			scoreMod = 0.5;
		}

		// loss
		else if (result == -1) {
			scoreMod = 0.0;
		}

		else {
			return targetTeamElo;
		}

		//Calculations
		double exponent = ( opponentElo - targetTeamElo ) / 2000;
		double expectedOutcome = (1 / (1 + (Math.pow(6, exponent))));
		newElo = (int) ( targetTeamElo + K * ( scoreMod - expectedOutcome ) );

		return newElo;
	}
	
	public static double calculateNewTeamBasedElo(double targetPlayerElo, double opponentAvgElo, int goalDifference) {
		double newElo = 0.0;
		double scoreMod = 0.5;
		int K = 250;
		
		// win
		if (goalDifference >= 1) {
			scoreMod = 0.5 + ( goalDifference * 0.05 );
			
			if(goalDifference > 10){
				scoreMod = 1.0;
			}
		}

		// draw
		else if (goalDifference == 0) {
			scoreMod = 0.5;
		}

		// loss ( negative goal diff )
		else {
			scoreMod = 0.5 + ( goalDifference * 0.05 );
			
			if(goalDifference < -10){
				scoreMod = 0.0;
			}
		}

		//Calculations
		double exponent = ( opponentAvgElo - targetPlayerElo ) / 1000;
		double expectedOutcome = (1 / (1 + (Math.pow(2, exponent))));
		newElo = targetPlayerElo + K * ( scoreMod - expectedOutcome );
		
		return newElo;
	}
}
