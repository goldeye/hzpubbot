package bot.duel.ranking;

import java.util.Comparator;

import bot.duel.PlayerDuelStats;

public class MyDuelComp implements Comparator<PlayerDuelStats> {
	 public int compare(PlayerDuelStats d1, PlayerDuelStats d2) {
	        if(d1.duelElo < d2.duelElo){
	            return 1;
	        } else {
	            return -1;
	        }
	    }
}
