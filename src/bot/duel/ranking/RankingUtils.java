package bot.duel.ranking;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import bot.BotGlobal;
import bot.duel.PlayerDuelStats;

public class RankingUtils {
	private static DecimalFormat df = new DecimalFormat("####0.00");

	/**
	 * Returns to the console list of the duel ranking scores in order of
	 * highest to lowest.
	 */
	public static List<String> orderPubDuelRanking(String alias) {
		
		List<PlayerDuelStats> stats = new LinkedList<PlayerDuelStats>();
		for(PlayerDuelStats playerStats:BotGlobal.playerPubDuelRatings){
			stats.add(playerStats);
		}
		
		Collections.sort(stats, new MyDuelComp());

		List<String> topDuelists = new ArrayList<String>();

		System.out.println("Sorted list entries: ");

		int duelRankPos = 1;
		String myRank = "Rank for: " + alias + " unknown";

		for (PlayerDuelStats e : BotGlobal.playerPubDuelRatings) {
			if (e.duelElo != 1000.0) {
				String response = duelRankPos + ") " + e.name + " --> " + df.format(e.duelElo);
				System.out.println(response);
				
				if(e.getName().toLowerCase().equals(alias.toLowerCase())){
					myRank = "YOU --> " + response;
				}

				if (duelRankPos <= 5) {
					topDuelists.add(response);
				}
				duelRankPos++;
			}
		}
		topDuelists.add(myRank);
		return topDuelists;
	}

	/**
	 * Returns to the console list of the public ranking scores in order of
	 * highest to lowest.
	 */
	public static List<String> orderPubRanking(String alias) {
		
		List<PlayerDuelStats> stats = new LinkedList<PlayerDuelStats>();
		for(PlayerDuelStats playerStats:BotGlobal.playerPubDuelRatings){
			stats.add(playerStats);
		}
		Collections.sort(stats, new MyPubComp());

		List<String> topDuelists = new ArrayList<String>();

		System.out.println("Sorted list entries: ");

		int pubRankPos = 1;
		String myRank = "Rank for: " + alias + " unknown";

		for (PlayerDuelStats e : BotGlobal.playerPubDuelRatings) {
			if (e.getPubElo() != 5000.0) {
				String response = pubRankPos + ") " + e.name + " --> " + df.format(e.getPubElo());
				System.out.println(response);

				if(e.getName().toLowerCase().equals(alias.toLowerCase())){
					myRank = "YOU --> " + response;
				}
				
				if (pubRankPos <= 5) {
					topDuelists.add(response);
				}
				pubRankPos++;
			}
		}
		topDuelists.add(myRank);
		return topDuelists;
	}
}
