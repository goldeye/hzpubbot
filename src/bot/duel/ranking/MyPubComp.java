package bot.duel.ranking;

import java.util.Comparator;

import bot.duel.PlayerDuelStats;

public class MyPubComp implements Comparator<PlayerDuelStats> {
	 public int compare(PlayerDuelStats d1, PlayerDuelStats d2) {
	        if(d1.getPubElo() < d2.getPubElo()){
	            return 1;
	        } else {
	            return -1;
	        }
	    }
}
