package bot.duel;

/**
 *
 * @author Alex
 */
public class Duel {
    public String name_home;
    public PlayerDuelStats duelistHome;
    public double start_rating_home;
    public double final_rating_home;
    
    public String name_away;
    public PlayerDuelStats duelistAway;
    public double start_rating_away;
    public double final_rating_away;    
}
