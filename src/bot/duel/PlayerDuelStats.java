package bot.duel;

import java.io.Serializable;

public class PlayerDuelStats implements Serializable {
	private static final long serialVersionUID = 4683492299857920489L;

	// public int id;
	public String name;
	public double duelElo;
	public double pubElo;
	// public double mini_elo;
	// public double scrim_elo;

	public boolean ignoreMe = false;

	public PlayerDuelStats(String name) {
		this.name = name;
		duelElo = 1000;
		pubElo = 5000;
		ignoreMe = false;
	}

	public PlayerDuelStats(String name, double duelElo, double pubElo, boolean ignoreMe) {
		this.name = name;
		this.duelElo = duelElo;
		this.pubElo = pubElo;
		this.ignoreMe = ignoreMe;
	}
	
	public PlayerDuelStats() {
	}

	public void setElo(double newElo) {
		this.duelElo = newElo;
	}

	public double getElo() {
		return duelElo;
	}
	
	public void setPubElo(double newElo) {
		this.pubElo = newElo;
	}

	public double getPubElo() {
		return pubElo;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public boolean getIgnoreStatus(){
		return ignoreMe;
	}
	
	public void setIgnoreStatus(boolean ignoreMe){
		this.ignoreMe = ignoreMe;
	}
}
