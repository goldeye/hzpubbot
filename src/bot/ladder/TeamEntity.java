package bot.ladder;


import java.io.Serializable;
import java.util.HashSet;

public class TeamEntity implements Serializable {
	private static final long serialVersionUID = 4683492299857920488L;

	//CORE
	public String teamName;
	public String teamOwner;
	public String captainPassword;
	public HashSet<String> rosteredPlayers = new HashSet<>();
	public HashSet<String> invitedPlayers = new HashSet<>();

	public double teamElo = 20000.0;

	public int gamesPlayed = 0;
	public int gamesWon = 0;
	public int gamesLost = 0;
	public int goalsScored = 0;
	public int goalsConceeded = 0;
	public String form = "";

	public TeamEntity(String teamName, String teamOwner){
		this.teamName = teamName;
		this.teamOwner = teamOwner;
		rosteredPlayers.add(teamOwner);
	}

	@Override
	public String toString(){
		return "TEAM:" + teamName + " OWNER:" + teamOwner + " PLAYERS:" + rosteredPlayers;
	}

	public void notch(String result, int homeScore, int awayScore) {
		if(form == null){
			form = "";
		}
		form = result + form;
	}

	public String getForm() {
		if(form==null){
			form = "";
		}
		if(form.length() <= 6) {
			return form;
		} else {
			return form.substring(0,6);
		}
	}
}
