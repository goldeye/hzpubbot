package bot.ladder;

import bot.BotGlobal;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public class TeamEntityManager implements Serializable {
    private static final long serialVersionUID = 4683492299857920486L;

    public static HashSet<TeamEntity> ladderTeams = new HashSet<>();

    public TeamEntityManager() { }

    private void setTeamData(HashSet<TeamEntity> ladderTeams) {
        this.ladderTeams = ladderTeams;
    }

    public int getNumberOfTeams() {
        return ladderTeams.size();
    }

    public String foundNewTeam(String teamName, String teamOwner) {
        TeamEntity teamEntity = new TeamEntity(teamName, teamOwner);

        for (TeamEntity entity : ladderTeams) {
            if (entity.teamOwner.equalsIgnoreCase(teamOwner)) {
                return (teamOwner + " ALREADY CAPTAINS A TEAM:" + teamEntity.teamName);
            }
        }

        for (TeamEntity entity : ladderTeams) {
            Iterator<String> rosteredPlayers = entity.rosteredPlayers.iterator();
            while (rosteredPlayers.hasNext()) {
                if (rosteredPlayers.next().equalsIgnoreCase(teamOwner)) {
                    return (teamOwner + " ALREADY IS ON TEAM: " + entity.teamName);
                }
            }
        }

        if (getTeam(teamName) == null) {
            ladderTeams.add(teamEntity);
            return ("CREATED TEAM:" + teamName);
        } else {
            return ("THAT TEAM NAME IS ALREADY TAKEN");
        }
    }

    public TeamEntity getTeam(String name) {
        for (TeamEntity teamEntity : ladderTeams) {
            if (teamEntity.teamName.equalsIgnoreCase(name)) {
                return teamEntity;
            }
        }
        System.out.println("NO SUCH TEAM EXISTS");
        return null;
    }

    public String getMyTeam(String player) {
        for (TeamEntity teamEntity : ladderTeams) {
            Iterator<String> rosteredPlayers = teamEntity.rosteredPlayers.iterator();
            while (rosteredPlayers.hasNext()) {
                if (rosteredPlayers.next().equalsIgnoreCase(player)) {
                    return (player + " IS ON TEAM: " + teamEntity.teamName);
                }
            }
        }
        System.out.println("NOT ON A TEAM");
        return "You are not on a team...";
    }

    public String getMyTeamStats(String player) {
        for (TeamEntity teamEntity : ladderTeams) {
            Iterator<String> rosteredPlayers = teamEntity.rosteredPlayers.iterator();
            while (rosteredPlayers.hasNext()) {
                if (rosteredPlayers.next().equalsIgnoreCase(player)) {
                    return (player + " IS ON TEAM: " + teamEntity.teamName
                            + " [ PLAYED:" + teamEntity.gamesPlayed
                            + " WON:" + teamEntity.gamesWon
                            + " LOST:" + teamEntity.gamesLost
                            + " GF:" + teamEntity.goalsScored
                            + " GA:" + teamEntity.goalsConceeded
                            + " ELO:" + teamEntity.teamElo
                            + " FORM:" + teamEntity.getForm()
                            + " ]");
                }
            }
        }
        System.out.println("NOT ON A TEAM");
        return "You are not on a team...";
    }

    public String getTeamStats(String teamName) {
        for (TeamEntity teamEntity : ladderTeams) {
            if (teamEntity.teamName.equalsIgnoreCase(teamName)) {
                return ("TEAM: " + teamEntity.teamName
                        + " [ PLAYED:" + teamEntity.gamesPlayed
                        + " WON:" + teamEntity.gamesWon
                        + " LOST:" + teamEntity.gamesLost
                        + " GF:" + teamEntity.goalsScored
                        + " GA:" + teamEntity.goalsConceeded
                        + " ELO:" + teamEntity.teamElo
                        + " FORM:" + teamEntity.getForm()
                        + " ]");
            }

        }
        System.out.println("NOT SUCH TEAM");
        return null;
    }

    public String getMyTeamName(String player) {
        for (TeamEntity teamEntity : ladderTeams) {
            Iterator<String> rosteredPlayers = teamEntity.rosteredPlayers.iterator();
            while (rosteredPlayers.hasNext()) {
                if (rosteredPlayers.next().equalsIgnoreCase(player)) {
                    return (teamEntity.teamName);
                }
            }
        }
        System.out.println("NOT ON A TEAM");
        return null;
    }

    private void renameTeam(String teamName, String delta) {
        for (TeamEntity teamEntity : ladderTeams) {
            if (teamEntity.teamName.equalsIgnoreCase(teamName)) {
                teamEntity.teamName = delta;
                System.out.println("TEAM: " + teamName + " --> " + delta);
            }
        }
        System.out.println("NO SUCH TEAM EXISTS");
    }

    private void setPassword(String teamName, String delta) {
        for (TeamEntity teamEntity : ladderTeams) {
            if (teamEntity.teamName.equalsIgnoreCase(teamName)) {
                teamEntity.captainPassword = delta;
                System.out.println("PASSWORD: " + teamName + " --> " + delta);
                return;
            }
        }
        System.out.println("NO SUCH TEAM EXISTS");
    }

    /**
     * Invites a player to your team
     *
     * @param captain
     *         target teams captain
     * @param playerName
     *         target player
     */
    public String[] invitePlayer(String captain, String playerName) {
        boolean match = false;
        for (TeamEntity teamEntity : ladderTeams) {
            if (teamEntity.teamOwner.equalsIgnoreCase(captain)) {
                Iterator<String> invitedPlayers = teamEntity.invitedPlayers.iterator();
                while (invitedPlayers.hasNext()) {
                    if (invitedPlayers.next().equalsIgnoreCase(playerName)) {
                        match = true;
                    }
                }
                if (!match) {
                    teamEntity.invitedPlayers.add(playerName);
                    System.out.println("INVITED: " + teamEntity.teamName + " --> " + playerName);
                    String[] feedback = {"INVITED: " + teamEntity.teamName + " --> " + playerName, teamEntity.teamName, teamEntity.teamOwner};
                    return feedback;
                } else {
                    System.out.println("Player already invited");
                    String[] feedback = {"That player has already been invited to your team"};
                    return feedback;
                }
            }
        }
        System.out.println("no team / target player supplied...");
        String[] feedback = {"ARE YOU ON A TEAM? Or was a target playername supplied?"};
        return feedback;
    }

    /**
     * Uninvites a player to your team
     *
     * @param captain
     *         target teams captain
     * @param playerName
     *         target player
     */
    public String cancelInvitePlayer(String captain, String playerName) {
        boolean match = false;
        for (TeamEntity teamEntity : ladderTeams) {
            if (teamEntity.teamOwner.equalsIgnoreCase(captain)) {
                Iterator<String> invitedPlayers = teamEntity.invitedPlayers.iterator();
                while (invitedPlayers.hasNext()) {
                    if (invitedPlayers.next().equalsIgnoreCase(playerName)) {
                        match = true;
                    }
                }
                if (match) {
                    teamEntity.invitedPlayers.remove(playerName);
                    System.out.println("CANCELLED INVITED OF: " + teamEntity.teamName + " --> " + playerName);
                    return ("CANCELLED INVITE: " + teamEntity.teamName + " --> " + playerName);
                } else {
                    System.out.println("Player was not on team invite list");
                    return ("That Player was not invited");
                }
            }
        }
        System.out.println("no team / target player supplied...");
        return ("ARE YOU ON A TEAM? Or was a target playername supplied?");
    }

    public LinkedList<String> getInvites(String playerName) {
        LinkedList<String> resultSet = new LinkedList<>();
        for (TeamEntity teamEntity : ladderTeams) {
            Iterator<String> invitedPlayers = teamEntity.invitedPlayers.iterator();
            while (invitedPlayers.hasNext()) {
                String invPlayerName = invitedPlayers.next();
                if (invPlayerName.equalsIgnoreCase(playerName)) {
                    System.out.println(playerName + " HAS AN INVITE TO: " + teamEntity.teamName);
                    resultSet.add(teamEntity.teamName);
                }
            }
        }
        return resultSet;
    }

    public LinkedList<String> getTeams() {
        LinkedList<String> resultSet = new LinkedList<>();
        for (TeamEntity teamEntity : ladderTeams) {
            resultSet.add(teamEntity.teamName);
        }
        return resultSet;
    }

    public LinkedList<String> getRoster(String teamName) {
        LinkedList<String> resultSet = new LinkedList<>();
        for (TeamEntity teamEntity : ladderTeams) {
            if (teamEntity.teamName.equalsIgnoreCase(teamName)) {
                resultSet.addAll(teamEntity.rosteredPlayers);
            }
        }
        return resultSet;
    }

    public String acceptInvite(String teamName, String playerName) {
        boolean match = false;
        String targetPlayer = ""; //gets exact spelling of said player.

        for (TeamEntity teamEntity : ladderTeams) {
            if (teamEntity.teamName.equalsIgnoreCase(teamName)) {
                Iterator<String> invitedPlayers = teamEntity.invitedPlayers.iterator();
                while (invitedPlayers.hasNext()) {
                    String invPlayerName = invitedPlayers.next();
                    if (invPlayerName.equalsIgnoreCase(playerName)) {
                        match = true;
                        targetPlayer = invPlayerName;
                    }
                }
                if (match) {
                    leaveTeam(playerName);
                    teamEntity.rosteredPlayers.add(playerName);
                    teamEntity.invitedPlayers.remove(targetPlayer);
                    System.out.println("ACCCEPTED INVITE: " + teamName + " --> " + playerName);
                    return "ACCCEPTED INVITE: " + teamName + " --> " + playerName;
                } else {
                    System.out.println("You have not been invited to: " + teamName);
                    return "You have not been invited to: " + teamName;
                }
            }
        }
        return "does the following team even exist?: " + teamName;
    }

    public String leaveTeam(String playerName) {
        TeamEntity targetTeam = null;
        for (TeamEntity teamEntity : ladderTeams) {
            Iterator<String> rosteredPlayers = teamEntity.rosteredPlayers.iterator();
            while (rosteredPlayers.hasNext()) {
                if (rosteredPlayers.next().equalsIgnoreCase(playerName)) {
                    targetTeam = teamEntity;
                }
            }
        }

        if (targetTeam != null) {
            targetTeam.rosteredPlayers.remove(playerName);
            System.out.println(playerName + " LEFT: " + targetTeam.teamName);
            return playerName + " LEFT: " + targetTeam.teamName;
        }
        return "You are not on a team...";
    }

    public LinkedList<TeamEntity> getLadder() {
        LinkedList resultList = new LinkedList();
        resultList = orderByElo();

        return resultList;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private static LinkedList<TeamEntity> orderByElo() {
        LinkedList<TeamEntity> sortableTeams = new LinkedList<>();
        sortableTeams.addAll(ladderTeams);

        sortableTeams.sort((Comparator) (o1, o2) -> {
            final Double x1 = ((TeamEntity) o1).teamElo;
            final Double x2 = ((TeamEntity) o2).teamElo;

            return x2.compareTo(x1);
        });
        return sortableTeams;
    }

    public void saveData(String targetFile) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(targetFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(ladderTeams);
            oos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveData() {
        saveData("teamentities.ser");
    }

    private void loadData(String targetFile) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(targetFile);
            ois = new ObjectInputStream(fis);
            HashSet<TeamEntity> anotherSet = (HashSet<TeamEntity>) ois.readObject();
            ois.close();

            setTeamData(anotherSet);

            //compatibility updates
            for (TeamEntity entity : anotherSet) {
                if (entity.teamElo == 0) {
                    entity.teamElo = 20000;
                    System.out.println("Updated Old Record of team: " + entity.teamName);
                }
            }

            System.out.println("TEAM DATA LOADED WITH:" + getNumberOfTeams() + " TEAMS");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void loadData() {
        loadData("teamentities.ser");
    }


    public static void main(String[] a) throws Exception {
//		TeamEntity entity = new TeamEntity();
//		TeamEntity entity2 = new TeamEntity();
//		entity.teamName = "test";
//		entity.teamOwner = "testowner";
//		entity.rosteredPlayers.add("player-1");
//		entity.rosteredPlayers.add("player-2");
//		entity.captainPassword = "password";
//		entity.invitedPlayers.add("player-3");
//
//		entity2.teamName = "test-2";
//		entity2.teamOwner = "testowner-2";
//		entity2.rosteredPlayers.add("player-4");
//		entity2.rosteredPlayers.add("player-5");
//		entity2.captainPassword = "password2";
//		entity2.invitedPlayers.add("player-6");
//
//		ladderTeams.add(entity);
//		ladderTeams.add(entity2);
//
//		FileOutputStream fos = new FileOutputStream("teamentities-test.ser");
//		ObjectOutputStream oos = new ObjectOutputStream(fos);
//		oos.writeObject(ladderTeams);
//		oos.close();
//
        FileInputStream fis = new FileInputStream("teamentities-test.ser");
        ObjectInputStream ois = new ObjectInputStream(fis);
        HashSet<TeamEntity> anotherSet = (HashSet<TeamEntity>) ois.readObject();
        ois.close();

        TeamEntityManager teamEntityManager = new TeamEntityManager();
        teamEntityManager.setTeamData(anotherSet);
//
//		System.out.println(anotherSet);
//		for(TeamEntity teamEntity:anotherSet){
//			System.out.println(teamEntity.toString());
//			System.out.println(teamEntity.invitedPlayers);
//		}
//
//		System.out.println("NUMBER OF TEAMS REGISTERED:" + teamEntityManager.getNumberOfTeams());
//
//		TeamEntity test = teamEntityManager.getTeam("TeSt");
//		System.out.println(test);
//
//		teamEntityManager.getTeam("TeSt-3");
//		teamEntityManager.renameTeam("test-2","test-2B");
        // teamEntityManager.saveData("teamentities-test.ser");
//        teamEntityManager.loadData("teamentities-test.ser");
//
//        for (TeamEntity teamEntity : TeamEntityManager.ladderTeams) {
//            System.out.println(teamEntity.toString());
//            System.out.println(teamEntity.invitedPlayers);
//        }
//
//        // teamEntityManager.invitePlayer("test-2", "plaYer-7");
//        teamEntityManager.invitePlayer("teSt-3", "player-7");
//
//        teamEntityManager.foundNewTeam("test-3C", "player-8");
//
//        teamEntityManager.acceptInvite("test-3", "player-7");
//
//        System.out.println(" * * * * ");
//        for (TeamEntity teamEntity : TeamEntityManager.ladderTeams) {
//            System.out.println(teamEntity.toString());
//            System.out.println(teamEntity.invitedPlayers);
//            System.out.println(teamEntity.teamElo);
//        }
//
//        teamEntityManager.getInvites("plAYer-3");
//        teamEntityManager.saveData("teamentities-test.ser");
//
//        ArenaEntity testArena = new ArenaEntity();
//        testArena.homePlayers.add("player-2");
//        testArena.homePlayers.add("player-1");
//        // testArena.awayPlayers.add("player-1");
//        testArena.homePlayers.add("playeR-2");
//        ArenaEntityManager testArenaEntityManager = new ArenaEntityManager(testArena);
//        testArenaEntityManager.assignFreqToTeam(teamEntityManager.getMyTeamName("player-BA"));
//
//        System.out.println(testArenaEntityManager.listFreqPlayers(0));

        String toParse = "Score:    Team Kara Thrace:9  Team exul:9";

        String[] subTokens = toParse.split(":");
        for (String str : subTokens) {
            System.out.println(str);
        }
        System.out.println(subTokens[2].charAt(0));
        System.out.println(subTokens[3].charAt(0));

        int test = Integer.parseInt(String.valueOf(subTokens[3].charAt(0)));
        System.out.println(test);

        System.out.println(teamEntityManager.getMyTeamStats("player-1"));

        BotGlobal.scrimArenaManager.targetArena.awayTeam = teamEntityManager.getTeam("test");

        BotGlobal.scrimArenaManager.targetArena.awayTeam.gamesLost++;

        System.out.println(teamEntityManager.getMyTeamStats("player-1"));

        String form = "";
        form = form + "W";
        System.out.println(form);
        form = form + "L";
        System.out.println(form);
    }
}
