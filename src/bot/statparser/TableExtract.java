package bot.statparser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TableExtract {

	// for stand alone testing at this time.
    public static void main(String[] args) throws InterruptedException {
    	HashSet<Player> players = new HashSet<>();

        getPlayerData(players);

        System.out.println("players:" + players.size());

        getTopGoals(players);
        getTopAssists(players);
        getTopShotAcc(players);
        getTopSavePcnt(players);
    }

    public static List<Player> getTopSavePcnt(HashSet<Player> players) {
        System.out.println(" # # Top SV% ( 10 Save minimum ) # # "); 
        List<Player> auxSubList = orderBySavePcnt(players);

//        for(Player player:players){
//        	auxSubList.add(player);
//        }

        for (Player aPerson : players) {
            if (aPerson.saves > 10) {
                auxSubList.add(aPerson);
            }
        }

        return getResultList(auxSubList);
    }

    public static List<Player> getTopShotAcc(HashSet<Player> players) {    
        System.out.println(" # # Top SH% ( 10 goal minimum ) # # ");
        List<Player> auxSubList = orderByShotAcc(players);

//        for(Player player:players){
//        	auxSubList.add(player);
//        }

        for (Player aPerson : players) {
            if (aPerson.goals > 2) {
                auxSubList.add(aPerson);
            }

        }

        return getResultList(auxSubList);
    }

    public static List<Player> getTopAssists(HashSet<Player> players) {
        System.out.println(" # # TOP ASSISTS # # ");
        List<Player> auxSubList = orderByAssists(players);

        auxSubList.addAll(players);

        return getResultList(auxSubList);
    }

    private static List<Player> getResultList(List<Player> auxSubList) {
        if (auxSubList.size() > 4) {
            return auxSubList.subList(0, 5);
        } else {
            return auxSubList.subList(0, auxSubList.size());
        }
    }

    public static List<Player> getTopGoals(HashSet<Player> players) {
        System.out.println(" # # TOP GOALS # # ");
        List<Player> auxSubList = orderByGoals(players);

        return getResultList(auxSubList);
    }

    public static List<Player> getTopChecks(HashSet<Player> players) {
        System.out.println(" # # TOP CHECKS # # ");
        List<Player> auxSubList = orderByChecks(players);
        auxSubList.addAll(players);

        return getResultList(auxSubList);
    }
    
    public static List<Player> getTopSteals(HashSet<Player> players) {
        System.out.println(" # # TOP STEALS # # ");
        List<Player> auxSubList = orderBySteals(players);
        auxSubList.addAll(players);

        return getResultList(auxSubList);
    }
    
    public static List<Player> getTopImpact(HashSet<Player> players) {
        System.out.println(" # # TOP IMPACT (+/-) # # ");
        List<Player> auxSubList = orderByGoalDif(players);
        auxSubList.addAll(players);

        return getResultList(auxSubList);
    }
    
    public static List<Player> getTopPuckTime(HashSet<Player> players) {
        System.out.println(" # # TOP PUCKTIME # # ");
        List<Player> auxSubList = orderByPuckTime(players);
        auxSubList.addAll(players);

        return getResultList(auxSubList);
    }

    public static HashSet<Player> getPlayerData(HashSet<Player> players) throws InterruptedException {
        String html;
        for (int page = 0; page < 5; page++) {
            CommonUrls urlGenerator = new CommonUrls();
            
            html = urlGenerator.generateUrl(1, page);
            System.out.println("Scraping: " + html);

            try {
                Thread.sleep(1000);
                Document doc = Jsoup.connect(html).get();
                Thread.sleep(2000);
                Elements tableElements = doc.select("table");

                Elements tableHeaderEles = tableElements.select("thead tr th");

                for (Element tableHeaderEle : tableHeaderEles) {
                    System.out.println(tableHeaderEle.text());
                }

                Elements tableRowElements = tableElements.select(":not(thead) tr");

                for (int i = 1; i < tableRowElements.size(); i++) {
                    Element row = tableRowElements.get(i);

                    Player player = new Player();
                    if (i >= 2) {
                        Elements rowItems = row.select("td");

                        for (int j = 0; j < rowItems.size(); j++) {

                            switch (j) {
                                case 0:
                                    player.name = rowItems.get(j).text();
                                    break;
                                case 1:
                                    player.rating = Double.parseDouble(rowItems.get(j).text());
                                    break;
                                case 2:
                                    if (!rowItems.get(j).text().contains("?")) {
                                        player.rpm = Double.parseDouble(rowItems.get(j).text());
                                    } else {
                                        player.rpm = 0;
                                    }
                                    break;
                                case 3:
                                    String[] playTime = (rowItems.get(j).text().split(":"));
                                    player.playTimeSeconds = (Double.parseDouble(playTime[0]) * 60) + Double.parseDouble(playTime[1]);
                                    break;
                                case 4:
                                    player.goals = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 5:
                                    player.assists = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 6:
                                    player.goalDif = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 7:
                                    player.shots = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 8:
                                    player.steals = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 9:
                                    player.turnovers = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 10:
                                    String[] puckTime = (rowItems.get(j).text().split(":"));
                                    player.puckTimeSeconds = (Double.parseDouble(puckTime[0]) * 60) + Double.parseDouble(puckTime[1]);
                                    break;
                                case 11:
                                    player.puckControls = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 12:
                                    player.checks = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 13:
                                    player.checked = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 14:
                                    String[] goalTime = (rowItems.get(j).text().split(":"));
                                    player.goalTime = (Double.parseDouble(goalTime[0]) * 60) + Double.parseDouble(goalTime[1]);
                                    break;
                                case 15:
                                    player.saves = Integer.parseInt(rowItems.get(j).text());
                                    break;
                                case 16:
                                    player.goalsAgainst = Integer.parseInt(rowItems.get(j).text());
                                    break;
                            }
                        }

                        calculateGoalRelatedStats(player);

                        calculateSaveRelatedStats(player);
                        
                        calculateStealRelatedStats(player);

                        if (!players.contains(player)) {
                            players.add(player);
                        }
                    }
                }

            } catch (IOException e) {
                page--;
                e.printStackTrace();
            }

            if (players.size() % 50 != 0) {
                page = 999;
            }
        }

        System.out.println("PLAYERS SCRAPED:" + players.size());
        return players;
    }

    @SuppressWarnings("unused")
	private static void dumpPlayerStats(Player player) {
        System.out.println("---- Outfield Stats ----");
        System.out.println(player.name);
        System.out.println("ra:" + player.rating);
        System.out.println("rpm:" + player.rpm);
        System.out.println("playtime:" + player.playTimeSeconds);
        System.out.println("goals:" + player.goals);
        System.out.println("assists:" + player.assists);
        System.out.println("goalDiff:" + player.goalDif);
        System.out.println("shots:" + player.shots);
        System.out.println("st:" + player.steals);
        System.out.println("tp:" + player.turnovers);
        System.out.println("pucktime:" + player.puckTimeSeconds);
        System.out.println("pc:" + player.puckControls);
        System.out.println("ch:" + player.checks);
        System.out.println("cd:" + player.checked);
        System.out.println("---- Goalie Stats ----");
        System.out.println("goalietime:" + player.goalTime);
        System.out.println("sv:" + player.saves);
        System.out.println("ga:" + player.goalsAgainst);
        System.out.println("- # - # - # - # - # -");
    }

    private static void calculateSaveRelatedStats(Player player) {
        if (player.saves > 0) {
            player.savePcnt = (player.saves * 100) / (player.saves + player.goalsAgainst);
        } else {
            player.savePcnt = 0;
        }
    }

    private static void calculateGoalRelatedStats(Player player) {
        if (player.goals > 0) {
            player.shotAcc = (player.goals * 100 / player.shots);
            player.goalsPerPeriod = 600 / (player.playTimeSeconds / player.goals);
            //            System.out.print("\n" + person.name + " : ");
            //            System.out.printf("Value: %.2f", person.goalsPerPeriod);
        } else {
            player.shotAcc = 0;
            player.goalsPerPeriod = 0;
        }
    }
    
    private static void calculateStealRelatedStats(Player player) {
        if (player.steals > 0) {
            player.stealsPerPeriod = 600 / (player.playTimeSeconds / player.steals);

            player.stealsPerPuckControls = (player.steals / player.puckControls) * 100;
            
            if(player.turnovers > 0){
            	player.stealsPerTurnover = player.steals / player.turnovers;
            }
        } 
        
        
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static List<Player> orderByGoals(HashSet<Player> players) {
    	List<Player> sortablePlayer = new ArrayList<>();
        sortablePlayer.addAll(players);
    	
        sortablePlayer.sort((Comparator) (o1, o2) -> {
            final Integer x1 = ((Player) o1).goals;
            final Integer x2 = ((Player) o2).goals;

            return x2.compareTo(x1);
        });
		return sortablePlayer;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static List<Player> orderByAssists(HashSet<Player> players) {
    	List<Player> sortablePlayer = new ArrayList<>();
        sortablePlayer.addAll(players);
    	
        sortablePlayer.sort((Comparator) (o1, o2) -> {
            final Integer x1 = ((Player) o1).assists;
            final Integer x2 = ((Player) o2).assists;

            return x2.compareTo(x1);
        });
		return sortablePlayer;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private static List<Player> orderByShotAcc(HashSet<Player> players) {
    	List<Player> sortablePlayer = new ArrayList<>();
        sortablePlayer.addAll(players);
    	
        sortablePlayer.sort((Comparator) (o1, o2) -> {
            final Integer x1 = ((Player) o1).shotAcc;
            final Integer x2 = ((Player) o2).shotAcc;

            return x2.compareTo(x1);
        });
		return sortablePlayer;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private static List<Player> orderBySavePcnt(HashSet<Player> players) {
    	List<Player> sortablePlayer = new ArrayList<>();
        sortablePlayer.addAll(players);
    	
        sortablePlayer.sort((Comparator) (o1, o2) -> {
            final Integer x1 = ((Player) o1).savePcnt;
            final Integer x2 = ((Player) o2).savePcnt;

            return x2.compareTo(x1);
        });
		return sortablePlayer;
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    private static List<Player> orderByChecks(HashSet<Player> players) {
    	List<Player> sortablePlayer = new ArrayList<>();
        sortablePlayer.addAll(players);

        sortablePlayer.sort((Comparator) (o1, o2) -> {
            final Integer x1 = ((Player) o1).checks;
            final Integer x2 = ((Player) o2).checks;

            return x2.compareTo(x1);
        });
		return sortablePlayer;
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    private static List<Player> orderBySteals(HashSet<Player> players) {
    	List<Player> sortablePlayer = new ArrayList<>();
        sortablePlayer.addAll(players);
    	
        sortablePlayer.sort((Comparator) (o1, o2) -> {
            final Integer x1 = ((Player) o1).steals;
            final Integer x2 = ((Player) o2).steals;

            return x2.compareTo(x1);
        });
		return sortablePlayer;
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    private static List<Player> orderByGoalDif(HashSet<Player> players) {
    	List<Player> sortablePlayer = new ArrayList<>();
        sortablePlayer.addAll(players);
    	
        sortablePlayer.sort((Comparator) (o1, o2) -> {
            final Integer x1 = ((Player) o1).goalDif;
            final Integer x2 = ((Player) o2).goalDif;

            return x2.compareTo(x1);
        });
		return sortablePlayer;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private static List<Player> orderByPuckTime(HashSet<Player> players) {
    	List<Player> sortablePlayer = new ArrayList<>();
        sortablePlayer.addAll(players);
    	
        sortablePlayer.sort((Comparator) (o1, o2) -> {
            final Double x1 = ((Player) o1).puckTimeSeconds;
            final Double x2 = ((Player) o2).puckTimeSeconds;

            return x2.compareTo(x1);
        });

		return sortablePlayer;
    }
}