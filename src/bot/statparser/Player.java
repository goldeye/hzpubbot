/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.statparser;

public class Player {

    public String name;
    public double rating;
    public double rpm;
    public double playTimeSeconds;
    public int goals;
    public int assists;
    public int goalDif = 0;
    public int shots;
    public int steals;
    public int turnovers;
    public double puckTimeSeconds;
    public int puckControls;
    public int checks;
    public int checked;
    //gk specific
    public double goalTime;
    public int saves;
    public int goalsAgainst;
    //
    //inferred
    //
    public int shotAcc;
    public int savePcnt;
    public int passAccPcnt;
    public int shotPerPuckControlPcnt;
    public double checksPerChecked;
    public double stealsPerTurnover;
    public double stealsPerPuckControls;
    public double turnoversPerPuckControls;
    public double goalsPerPeriod;
    public double assistsPerPeriod;
    public double checksPerPeriod;
    public double checkedPerPeriod;
    public double stealsPerPeriod;
    public double turnoversPerPeriod;
    public double shotsPerPeriod;
    public double shotsPerTurnover;
    public double puckControlsPerPeriod;
    public double hogTimePerPuckControl;
    public double hogTimePerPeriod;
    //gk specific
    public double savesPerPeriod;
    public double goalsAgainstPerPeriod;
    
	public int getGoalDif() {
		return goalDif;
	}
}