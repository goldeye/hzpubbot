package bot.statparser;

public class CommonUrls {
	int indexVal;
	final String PUBLIC_FLAG = "&pub=1";
	final int RECORDS_PER_PAGE = 50;
    
	//PUBLIC COMMON HTML STRING
    final String PUBLIC_COMMON_STATS = "http://rshl.org/ci/index.php?c=statsview&LS=113&suf=&col=Rating&desc=1&a=";
    //PUBLIC COMMON GOALIE HTML
    final String PUBLIC_GOALIE_STATS = "http://rshl.org/ci/index.php?c=statsview&LS=123&suf=goalie&col=GoalieRating&desc=1&a=";
    	
    public String generateUrl(int statType, int page){
    	String url = "";
    	if(statType == 1){
    		indexVal = page * RECORDS_PER_PAGE;
    		
    		url = PUBLIC_COMMON_STATS + indexVal + PUBLIC_FLAG;
    		System.out.println(url);
    	}
    	
    	if(statType == 2){
    		indexVal = page * RECORDS_PER_PAGE;
    		
    		url = PUBLIC_GOALIE_STATS + indexVal + PUBLIC_FLAG;
    		System.out.println(url);
    	}

    	return url;
    }
}
