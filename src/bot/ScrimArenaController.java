package bot;

import bot.duel.EloCalc;
import bot.ladder.TeamEntity;
import bot.statparser.Player;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * The controller implements the ChatnetSocketListener and deals with the logic
 * for user commands.
 *
 * @author Fur - framework by Nax
 */
public class ScrimArenaController implements ChatnetSocketListener {
    public static final String LOSING_BY_THREE_OR_MORE
            = " have conceded again, will we see a comeback?";
    public int replies = 0;

    ChatnetUtils utils = new ChatnetUtils();
    String BOT_ADMIN = "furry+";
    private ChatnetSocket socket;

    private HashSet<Player> players;

    private int homeScore = 0;
    private int awayScore = 0;

    ScrimArenaController(HashSet<Player> players2) {
        this.players = players2;
        System.out.println("PLAYERS IN DB:" + players2.size());
    }


    @Override
    public void processChatnetMessage(ChatnetSocket socket, String message, String source) throws IOException {

        if (this.socket == null) {
            this.socket = socket;
        }


        // DEBUG
        // System.out.println("SCRIM IS PROCESSING:" + source + ":HZ:" + message);

		/*
         * utils.chatnetTokenize() splits chatnet messages by their different
		 * sections delimited by colon characters Refer to:
		 * http://wiki.minegoboom.com/index.php/ChatNet_Protocol
		 */
        String[] tokens = utils.chatnetTokenize(message);

        // If message is a !command pm'ed to this bot
        if (tokens != null) {
            validateArenaEntree(tokens);
            updateScrimPlayers(tokens);
            processUserCommand(socket, tokens);
            processScrimArenaMessage(tokens);
        } else {
            System.out.println("SCRIM CONTROLLER GOT AN EMPTY MESSAGE...");
        }
    }

    private void updateScrimPlayers(String[] tokens) {
        if (tokens[0].equals("SHIPFREQCHANGE")) {
            if (tokens[3].equals("6") || tokens[3].equals("7")) {
                System.out.println(tokens[1] + " MOVED TO OR SHIPCHANGED TO SCRIM ARENA");
                if (tokens[3].equals("6")) {
                    BotGlobal.scrimArenaManager.addPlayer(tokens[1], 0);
                }

                if (tokens[3].equals("7")) {
                    BotGlobal.scrimArenaManager.addPlayer(tokens[1], 1);
                }
            } else {
                System.out.println(tokens[1] + " WENT SOMEWHERE THAT ISNT SCRIM");
                BotGlobal.scrimArenaManager.removePlayer(tokens[1]);
            }
        }

        if (tokens[0].equals("LEAVING")) {
            System.out.println(tokens[1] + " DETECTED LEAVING ARENA - SCRIM CONTROLLER");
            BotGlobal.scrimArenaManager.removePlayer(tokens[1]);
        }
    }

    private void validateArenaEntree(String[] tokens) {
        if (tokens[0].equals("PLAYER")) {
            System.out.println(tokens[1] + " DETECTED IN ARENA BY SCRIM CONTROLLER");
            if (tokens[3].equals("6")) {
                BotGlobal.scrimArenaManager.addPlayer(tokens[1], 0);
            }
            if (tokens[3].equals("7")) {
                BotGlobal.scrimArenaManager.addPlayer(tokens[1], 1);
            }
        }
    }

    private void processScrimArenaMessage(String[] tokens) throws IOException {
        if (tokens[0].equals("MSG") && tokens[1].equals("ARENA")) {
            System.out.println("SCRIM ARENA CONT:" + tokens[2]);
            updateScore(tokens);
        }
    }

    private void updateScore(String[] tokens) throws IOException {
        if (tokens[0].equals("MSG") && tokens[1].equals("ARENA")) {
            if (tokens[2].contains("Score:")) {
                System.out.println("UPDATING SCORE...");
                String[] subTokens = tokens[2].split(":");
                System.out.println(subTokens);

                System.out.println("HOME SCORE: " + subTokens[2].charAt(0));
                homeScore = Integer.parseInt(String.valueOf(subTokens[2].substring(0, 1)));
                System.out.println("AWAY SCORE: " + subTokens[3].charAt(0));
                awayScore = Integer.parseInt(String.valueOf(subTokens[3].substring(0, 1)));

                if(homeScore - 2 > awayScore){
                    String awayTeamName = "FREQ 7";
                    if(BotGlobal.scrimArenaManager.targetArena.awayTeam != null){
                        awayTeamName = BotGlobal.scrimArenaManager.targetArena.awayTeam.teamName;
                    }
                    socket.send("SEND:PUB:" + awayTeamName + LOSING_BY_THREE_OR_MORE);
                }

                if(awayScore -2 > homeScore){
                    String homeTeamName = "FREQ 6";
                    if(BotGlobal.scrimArenaManager.targetArena.homeTeam != null){
                        homeTeamName = BotGlobal.scrimArenaManager.targetArena.homeTeam.teamName;
                    }
                    socket.send("SEND:PUB:" + homeTeamName + LOSING_BY_THREE_OR_MORE);
                }
            }

            if (tokens[2].contains("GAME OVER!")) {
                System.out.println("FINAL SCORE: " + homeScore + " - " + awayScore);

                // UPDATE TEAM ENTITIES
                if (homeScore > awayScore) {
                    if (BotGlobal.scrimArenaManager.targetArena.homeTeam != null) {
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.gamesWon++;
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.gamesPlayed++;
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.goalsScored = homeScore + BotGlobal.scrimArenaManager.targetArena.homeTeam.goalsScored;
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.goalsConceeded = awayScore + BotGlobal.scrimArenaManager.targetArena.homeTeam.goalsConceeded;

                        BotGlobal.scrimArenaManager.targetArena.homeTeam.notch("W", homeScore, awayScore);
                        socket.send("SEND:PUB:" + BotGlobal.scrimArenaManager.targetArena.homeTeam.teamName + " has just won a LADDER MATCH" );
                        if(BotGlobal.scrimArenaManager.targetArena.homeTeam.form.startsWith("WWW")){
                            socket.send("SEND:PUB:" + BotGlobal.scrimArenaManager.targetArena.homeTeam.teamName + " is on a winning streak, who can stop them!" );

                        }
                        // BotGlobal.scrimArenaManager.targetArena.homeTeam.teamElo = BotGlobal.scrimArenaManager.targetArena.homeTeam.teamElo + (10 * (homeScore - awayScore));
                    }
                    if (BotGlobal.scrimArenaManager.targetArena.awayTeam != null) {
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.gamesLost++;
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.gamesPlayed++;
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.goalsScored = awayScore + BotGlobal.scrimArenaManager.targetArena.awayTeam.goalsScored;
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.goalsConceeded = homeScore + BotGlobal.scrimArenaManager.targetArena.awayTeam.goalsConceeded;

                        BotGlobal.scrimArenaManager.targetArena.awayTeam.notch("L", homeScore, awayScore);
                        socket.send("SEND:PUB:" + BotGlobal.scrimArenaManager.targetArena.awayTeam.teamName + " has just lost a LADDER MATCH" );
                        if(BotGlobal.scrimArenaManager.targetArena.awayTeam.form.startsWith("LLL")){
                            socket.send("SEND:PUB:" + BotGlobal.scrimArenaManager.targetArena.awayTeam.teamName + " is on a losing streak, to the pit of misery, dilly dilly." );

                        }
                        // BotGlobal.scrimArenaManager.targetArena.awayTeam.teamElo = BotGlobal.scrimArenaManager.targetArena.awayTeam.teamElo - (10 * (homeScore - awayScore));
                    }

                } else {
                    if (BotGlobal.scrimArenaManager.targetArena.homeTeam != null) {
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.gamesLost++;
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.gamesPlayed++;
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.goalsScored = homeScore + BotGlobal.scrimArenaManager.targetArena.homeTeam.goalsScored;
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.goalsConceeded = awayScore + BotGlobal.scrimArenaManager.targetArena.homeTeam.goalsConceeded;

                        BotGlobal.scrimArenaManager.targetArena.homeTeam.notch("L", homeScore, awayScore);
                        socket.send("SEND:PUB:" + BotGlobal.scrimArenaManager.targetArena.homeTeam.teamName + " has just lost a LADDER MATCH" );
                        if(BotGlobal.scrimArenaManager.targetArena.homeTeam.form.startsWith("LLL")){
                            socket.send("SEND:PUB:" + BotGlobal.scrimArenaManager.targetArena.homeTeam.teamName + " is on a losing streak, to the pit of misery, dilly dilly." );

                        }
                        // BotGlobal.scrimArenaManager.targetArena.homeTeam.teamElo = BotGlobal.scrimArenaManager.targetArena.homeTeam.teamElo + (10 * (homeScore - awayScore));

                    }
                    if (BotGlobal.scrimArenaManager.targetArena.awayTeam != null) {
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.gamesWon++;
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.gamesPlayed++;
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.goalsScored = awayScore + BotGlobal.scrimArenaManager.targetArena.awayTeam.goalsScored;
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.goalsConceeded = homeScore + BotGlobal.scrimArenaManager.targetArena.awayTeam.goalsConceeded;

                        socket.send("SEND:PUB:" + BotGlobal.scrimArenaManager.targetArena.awayTeam.teamName + " has just won a LADDER MATCH" );
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.notch("W", homeScore, awayScore);
                        if(BotGlobal.scrimArenaManager.targetArena.awayTeam.form.startsWith("WWW")){
                            socket.send("SEND:PUB:" + BotGlobal.scrimArenaManager.targetArena.awayTeam.teamName + " is on a winning streak, who can stop them!" );
                        }
                        // BotGlobal.scrimArenaManager.targetArena.awayTeam.teamElo = BotGlobal.scrimArenaManager.targetArena.awayTeam.teamElo - (10 * (homeScore - awayScore));
                    }
                }

                //Calc and Update Elo 2.0
                double homeElo = 20000;
                double awayElo = 20000;

                if (BotGlobal.scrimArenaManager.targetArena.homeTeam != null) {
                    homeElo = BotGlobal.scrimArenaManager.targetArena.homeTeam.teamElo;
                }

                if (BotGlobal.scrimArenaManager.targetArena.awayTeam != null) {
                    awayElo = BotGlobal.scrimArenaManager.targetArena.awayTeam.teamElo;
                }

                if (BotGlobal.scrimArenaManager.targetArena.homeTeam != null) {
                    if (homeScore > awayScore) {
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.teamElo = EloCalc.calculateNewTeamElo(homeElo, awayElo, 1);
                    } else {
                        BotGlobal.scrimArenaManager.targetArena.homeTeam.teamElo = EloCalc.calculateNewTeamElo(homeElo, awayElo, -1);
                    }
                }

                if (BotGlobal.scrimArenaManager.targetArena.awayTeam != null) {
                    if (awayScore > homeScore) {
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.teamElo = EloCalc.calculateNewTeamElo(awayElo, homeElo, 1);
                    } else {
                        BotGlobal.scrimArenaManager.targetArena.awayTeam.teamElo = EloCalc.calculateNewTeamElo(awayElo, homeElo, -1);
                    }
                }

                // reset values
                homeScore = 0;
                awayScore = 0;
                BotGlobal.scrimArenaManager.targetArena.homeTeam = null;
                BotGlobal.scrimArenaManager.targetArena.awayTeam = null;

                System.out.println("--- UPDATING SER FILE ---");
                BotGlobal.ladderTeamManager.saveData();
                System.out.println("--- WRITING BACKUP SER FILE ---");
                BotGlobal.ladderTeamManager.saveData("ScrimLadder"+System.currentTimeMillis()+".ser");

                socket.send("SEND:PUB:Get your team in scrim and !registerteam to play" );
            }
        }
    }

    private void processUserCommand(ChatnetSocket socket, String[] tokens) throws IOException {
        if (tokens[0].equals("MSG") && tokens[1].equals("PRIV") && tokens[3].startsWith("!")) {

            String targetPlayer = tokens[2];
            String command = tokens[3].trim();
            String suppliedValue = null;

            System.out.println("PARSING COMMAND:" + command);

            command = command.trim();
            String[] commandParts = command.split(" ");
            command = commandParts[0];
            command = command.toLowerCase();


            if (commandParts.length > 1) {
                suppliedValue = commandParts[1];
                if (commandParts.length > 2) {
                    for (int i = 2; i < commandParts.length; i++) {
                        suppliedValue = suppliedValue + " " + commandParts[i];
                    }
                }
                System.out.println(suppliedValue);
            }

            System.out.println("PASSING COMMAND-->" + command + " :TARGET PLAYER -->" + targetPlayer);

            switch (command) {
                case "!help":
                    socket.send("SEND:PRIV:" + tokens[2] + ":"
                                        + "Scrim Ladder Bot Commands... !teams, !roster <teamName>, !foundteam <teamName>, " +
                                        "!offers, !invite <player>, !cancelinvite <player>, !accept <teamname>, !leaveteam, " +
                                        "!teamstats <teamname>, !myteam, !myteamstats, !registerteam, !ladder");
                    break;

                case "!owner":
                    socket.send("SEND:PRIV:" + tokens[2] + ":" + "The owner of this bot is..." + BOT_ADMIN);
                    break;

                case "!offers":
                    LinkedList<String> resultSet = BotGlobal.ladderTeamManager.getInvites(tokens[2]);
                    if (resultSet.size() > 0) {
                        for (String result : resultSet) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + "Active Invitation for: " + result);
                        }
                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + "you have no invites pending...");
                    }
                    break;

                case "!teams":
                    LinkedList<String> teamList = BotGlobal.ladderTeamManager.getTeams();
                    if (teamList.size() > 0) {
                        for (String result : teamList) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + result);
                        }
                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + "no teams exist!");
                    }
                    break;

                case "!roster":
                    LinkedList<String> rosterList = BotGlobal.ladderTeamManager.getRoster(suppliedValue);
                    if (rosterList.size() > 0) {
                        for (String result : rosterList) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + result);
                        }
                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + suppliedValue + " does not exist...");
                    }
                    break;

                case "!teamstats":
                    String teamStatsResult = BotGlobal.ladderTeamManager.getTeamStats(suppliedValue);
                    if (teamStatsResult != null) {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + teamStatsResult);

                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + suppliedValue + " does not exist...");
                    }
                    break;

                case "!foundtean":
                case "!foundteam":
                    if (suppliedValue != null) {
                        String feedback = BotGlobal.ladderTeamManager.foundNewTeam(suppliedValue, tokens[2]);
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + feedback);

                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + "you did not provide a team name !foundteam teamname");
                    }
                    break;

                case "!invite":
                    if (suppliedValue != null) {
                        String[] feedback = BotGlobal.ladderTeamManager.invitePlayer(tokens[2], suppliedValue);
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + feedback[0]);
                        if (feedback.length > 1) {
                            socket.send("SEND:PRIV:" + suppliedValue + ":" + "You have been invited to: " + feedback[1] + " BY: " + feedback[2]);
                        }
                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + "you did not provide a player name !invite player ");
                    }
                    break;

                case "!cancelinvite":
                    if (suppliedValue != null) {
                        String feedback = BotGlobal.ladderTeamManager.cancelInvitePlayer(tokens[2], suppliedValue);
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + feedback);

                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + "you did not provide a player name !invite player ");
                    }
                    break;

                case "!accept":
                    if (suppliedValue != null) {
                        String feedback = BotGlobal.ladderTeamManager.acceptInvite(suppliedValue, tokens[2]);
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + feedback);
                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + "you did not provide a team name !accept teamname)");
                    }
                    break;

                case "!registerteam":
                    if (suppliedValue == null){
                        suppliedValue = BotGlobal.ladderTeamManager.getMyTeamName(tokens[2]);
                    }
                    if (suppliedValue != null) {
                        int feedback = BotGlobal.scrimArenaManager.assignFreqToTeam(suppliedValue, tokens[2]);

                        if (feedback == -1) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + " You are not on that team...");
                        }

                        if (feedback == 0) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + " no team players on any freq.");
                        }

                        if (feedback == 69) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + " You can not assign your team to a scrim cage match with rostered players on both frequencies...");
                        }

                        if (feedback == 6) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + " You have been registered as FREQ 6");
                            socket.send("SEND:PUB:FREQ 6 has been claimed by:" + suppliedValue);
                        }

                        if (feedback == 7) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + " You have been registered as FREQ 7");
                            socket.send("SEND:PUB:FREQ 7 has been claimed by:" + suppliedValue);
                        }

                        if (feedback == 4) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + " You need 4 players from one team on one freq to register as playing.");
                        }
                    } else {
                        socket.send("SEND:PRIV:" + tokens[2] + ":" + "you did not provide a team name !registerteam teamname");
                    }
                    break;

                case "!leaveteam":
                    String feedback = BotGlobal.ladderTeamManager.leaveTeam(tokens[2]);
                    socket.send("SEND:PRIV:" + tokens[2] + ":" + feedback);
                    break;

                case "!myteam":
                    String myTeam = BotGlobal.ladderTeamManager.getMyTeam(tokens[2]);
                    socket.send("SEND:PRIV:" + tokens[2] + ":" + myTeam);
                    break;

                case "!myteamstats":
                    String myTeamStats = BotGlobal.ladderTeamManager.getMyTeamStats(tokens[2]);
                    socket.send("SEND:PRIV:" + tokens[2] + ":" + myTeamStats);
                    break;

                case "!save":
                    if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
                        System.out.println("--- SAVING SER FILE ---");
                        BotGlobal.ladderTeamManager.saveData("ScrimLadder"+System.currentTimeMillis()+".ser");
                    }
                    break;

                case "!load":
                    if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
                        System.out.println("--- LOADING SER FILE ---");
                        BotGlobal.ladderTeamManager.loadData();
                    }
                    break;

                case "!debug":
                    if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
                        System.out.println(BotGlobal.scrimArenaManager.listFreqPlayers(0));
                        System.out.println(BotGlobal.scrimArenaManager.listFreqPlayers(1));
                    }
                    break;

                case "!ladder":
                    LinkedList<TeamEntity> ladderRanks = BotGlobal.ladderTeamManager.getLadder();
                    socket.send("SEND:PRIV:" + tokens[2] + ":--- SCRIM LADDER STANDINGS ---");
                    int rankNumber = 1;
                    for (TeamEntity rankedTeamData : ladderRanks) {
                        if (rankedTeamData.gamesPlayed != 0) {
                            socket.send("SEND:PRIV:" + tokens[2] + ":" + rankNumber++ + ") " + rankedTeamData.teamName + " --> " + rankedTeamData.teamElo);
                        }
                    }
                    break;

                case "!publishladder":
                    if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
                        LinkedList<TeamEntity> ladderRanks2 = BotGlobal.ladderTeamManager.getLadder();
                        socket.send("SEND:PUB:" + ":--- SCRIM LADDER STANDINGS ---");
                        int rankNumber2 = 1;
                        for (TeamEntity rankedTeamData : ladderRanks2) {
                            if (rankedTeamData.gamesPlayed != 0) {
                                socket.send("SEND:PUB:" + rankNumber2++ + ") " + rankedTeamData.teamName + " --> " + rankedTeamData.teamElo);
                            }
                        }
                    }
                    break;

                case "!say":
                    if (tokens[2].toLowerCase().equals(BOT_ADMIN.toLowerCase())) {
                        socket.send("SEND:PUB:" + suppliedValue);
                    }
                    break;

                default:
                    socket.send("SEND:PRIV:" + tokens[2] + ":" + "NOT A VALID COMMAND -->" + command);
            }

            System.out.println("--- UPDATING SER FILE ---");
            BotGlobal.ladderTeamManager.saveData();
        }
    }


    @Override
    public void chatnetSocketHasClosed(ChatnetSocket socket) {
        // TODO Auto-generated method stub
    }
}