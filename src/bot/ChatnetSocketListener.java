package bot;

import java.io.IOException;

/**
 * Interface required for classes to listen to SubSpace socket
 * @author Nax
 */
public interface ChatnetSocketListener {
	void processChatnetMessage(ChatnetSocket socket, String message, String source) throws IOException;
	void chatnetSocketHasClosed(ChatnetSocket socket);
}
