package bot.arena;

import bot.ladder.TeamEntity;
import java.util.HashSet;

public class ArenaEntity {
    public TeamEntity homeTeam;
    public TeamEntity awayTeam;

    public HashSet<String> homePlayers = new HashSet<>();
    public HashSet<String> awayPlayers = new HashSet<>();

    public ArenaEntity(){}
}
