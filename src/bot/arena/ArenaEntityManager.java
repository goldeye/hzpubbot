package bot.arena;

import bot.BotGlobal;

import java.util.HashSet;
import java.util.LinkedList;

public class ArenaEntityManager {

    public ArenaEntity targetArena;

    public ArenaEntityManager(ArenaEntity targetArena) {
        this.targetArena = targetArena;
    }

    public void addPlayer(String player, int targetTeam) {
        if (targetTeam == 0) {
            targetArena.homePlayers.add(player);
            targetArena.awayPlayers.remove(player);
        } else if (targetTeam == 1) {
            targetArena.awayPlayers.add(player);
            targetArena.homePlayers.remove(player);
        }
    }

    public void removePlayer(String player) {
        targetArena.awayPlayers.remove(player);
        targetArena.homePlayers.remove(player);
    }

    public String listFreqPlayers(int targetTeam) {
        if (targetTeam == 0) {
            return targetArena.homePlayers.toString();
        } else if (targetTeam == 1) {
            return targetArena.awayPlayers.toString();
        } else
            return ("INVALID VALUE SUPPLIED");
    }

    public int assignFreqToTeam(String targetTeam, String token) {
        int homeTeamActivePlayers = 0;
        int awayTeamActivePlayers = 0;

        LinkedList<String> roster = BotGlobal.ladderTeamManager.getRoster(targetTeam);
        boolean onRoster = false;

        System.out.println("token val:" + token);
        for (String rosteredPlayer : roster) {
            System.out.println("checking against:" + rosteredPlayer);
            if (rosteredPlayer.equalsIgnoreCase(token)) {
                System.out.println("PLAYER CONFIRMED ON ROSTER");
                onRoster = true;
            }
        }

        if(!onRoster){
            return -1;
        }

        HashSet<String> currentHomePlayers = targetArena.homePlayers;
        HashSet<String> currentAwayPlayers = targetArena.awayPlayers;

        for (String activeHomePlayer : currentHomePlayers) {
            for (String rosteredPlayer : roster) {
                if (rosteredPlayer.equalsIgnoreCase(activeHomePlayer)) {
                    homeTeamActivePlayers++;
                }
            }
        }

        for (String activeAwayPlayer : currentAwayPlayers) {
            for (String rosteredPlayer : roster) {
                if (rosteredPlayer.equalsIgnoreCase(activeAwayPlayer)) {
                    awayTeamActivePlayers++;
                }
            }
        }

        if (homeTeamActivePlayers == 0 && awayTeamActivePlayers == 0) {
            System.out.println("Your team has no players on any frequency");
            return 0;
        }

        if (homeTeamActivePlayers > 0 && awayTeamActivePlayers > 0) {
            System.out.println("You can not assign your team to a scrim cage match with rostered players on both frequencies...");
            return 69;
        }

        if (homeTeamActivePlayers > awayTeamActivePlayers) {
            System.out.println("found: " + targetTeam + " on home freq. PLAYERS FOUND:" + homeTeamActivePlayers);
            if(homeTeamActivePlayers > 3) {
                targetArena.homeTeam = BotGlobal.ladderTeamManager.getTeam(targetTeam);
                return 6;
            } else {
                System.out.println("YOU NEED 4 OF YOUR PLAYERS ON A FREQ TO DECLARE IT AS YOUR TEAM...");
                return 4;
            }
        } else {
            System.out.println("found: " + targetTeam + " on away freq. PLAYERS FOUND:" + awayTeamActivePlayers);
            if(awayTeamActivePlayers > 3) {
                targetArena.awayTeam = BotGlobal.ladderTeamManager.getTeam(targetTeam);
                return 7;
            } else {
                System.out.println("YOU NEED 4 OF YOUR PLAYERS ON A FREQ TO DECLARE IT AS YOUR TEAM...");
                return 4;
            }
        }
    }


}
